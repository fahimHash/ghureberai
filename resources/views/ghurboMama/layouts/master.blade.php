<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- toChange --}}
    <meta property="og:url"           content="http://www.your-domain.com/your-page.html" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Your Website Title" />
    <meta property="og:description"   content="Your description" />
    <meta property="og:image"         content="http://www.your-domain.com/path/image.jpg" />

    
    <title>Recommandation System</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ URL::asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i|Roboto:400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('/css/custom.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/jquery.modal.min.css') }}">

    <style type="text/css">
        .form-control::-webkit-input-placeholder { color: white;  }
        .form-control:-moz-placeholder { color: white; }
        .form-control::-moz-placeholder { color: white;  }
        .form-control:-ms-input-placeholder { color: white; }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  @yield('headScripts')

</head>

<body>

    @include('ghurboMama.partials.navigation')
    @include('ghurboMama.partials.alert')
    
    @yield('content')
    <hr>
    <footer class="section section-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h4>Footer Header</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisici elit, <br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostrud</p>
                </div>
                <div class="col-sm-6">
                    <p class="text-info text-right"> <br><br></p>
                    <div class="row">
                        
                        
                        
                    
                        <div class="col-md-12 hidden-lg hidden-md hidden-sm text-left"> <a href="#"><i class="fa fa-2x fa-fw fa-instagram text-inverse"></i></a> <a href="#"><i class="fa fa-2x fa-fw fa-twitter text-inverse"></i></a> <a href="#"></a> <a href="#"><i class="fa fa-2x fa-fw fa-github text-inverse"></i></a>                            </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 hidden-xs text-right"> <a href="#"><i class="fa fa-2x fa-fw fa-instagram text-inverse"></i></a> <a href="#"><i class="fa fa-2x fa-fw fa-twitter text-inverse"></i></a> 

                        {{-- toChange --}}
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode('https://developers.google.com/chart/interactive/docs/gallery') }}" target='_blank'><i class="fa fa-2x fa-fw fa-facebook text-inverse"></i></a> 

                        <a href="#"><i class="fa fa-2x fa-fw fa-github text-inverse"></i></a>                            </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ URL::asset('/js/jq.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ URL::asset('/js/bootstrap.min.js') }}"></script>

    <script src="{{ URL::asset('/js/price.slider.js') }}"></script>
    <script src="{{ URL::asset('/js/rating.star.js') }}"></script>
    <script src="{{ URL::asset('/js/jquery.modal.min.js') }}"></script>
    
    <script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-Ft8S9iuQgDsH_kLT4GLZyQBUxuQPF3A&libraries=visualization">
    </script>

    

    @yield('script')

    

</body>

</html>