@extends('ghurboMama.layouts.master')

@section('content')

	<div class="container">
	    <div class="row">
	    	{{-- map --}}
	    	<div id="gmap" style="display: none"></div>
	    	{{-- map --}}
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Dashboard</div>

	                <div class="panel-body">
	                    <h3>Admin Panel</h3>
	                    <hr>
	                    <h4 style="background-color: #f2f2f2;padding: 15px">Locations</h4>
	                    <div>
	                    	<a class="btn btn-success" href="{{route('addLocation')}}">Add Location</a>
	                    	<a class="btn btn-primary" href="{{route('updateLocation')}}">Update Location</a>
	                    	<a class="btn btn-danger" href="#">Delete Location</a>
	                    </div>
	                    <hr>

	                    <h4 style="background-color: #f2f2f2;padding: 15px">Visitors</h4>

	                    <div>
	                    	<h4 style="color: #bfbfbf">Total Visitors <span style="color:#00B22D;font-size: 2.5em">{{$visitorCount}}</span></h4>
	                    	
	                    </div>
	                    <br>
	                    <div>
	                    	<a class="btn btn-success" target="blank" 
	                    	href="{{route('admin.visitor.heatmap')}}">Site Access Points</a>

	                    	<a class="btn btn-primary" target="blank" 
	                    	href="{{route('admin.visitor.chartDivision')}}">Division Chart</a>
	                    </div>
	                    <br>
	                    <div>
	                    	<h4 style="color: #bfbfbf">Select A Division</h4>
	                    	<form class="form" method="get" action="{{route('admin.visitor.division.areas')}}">
	                    		<select class="form-control" name="visitor-div-area">
								  <option selected>Dhaka</option>
								  <option>Mymensingh</option>
								  <option>Khulna</option>
								  <option>Rajshahi</option>
								  <option>Chittagong</option>
								  <option>Barisal</option>
								  <option>Sylhet</option>
								  <option>Rangpur</option>
								</select>
								<button style="margin-top: 15px" type="submit" class="btn btn-primary" name="form-submit">See Areas</button>
								{{ csrf_field() }}
	                    	</form>

	                    </div>
	                    <hr>
	                    
	                    <h4 style="background-color: #f2f2f2;padding: 15px">Users</h4>
	                    <h4 style="color: #bfbfbf">Total Users <span style="color:#00B22D;font-size: 2.5em">{{$userCount}}</span></h4>
	                    <a class="btn btn-success" href="{{route('admin.user.users')}}">All Users</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection

