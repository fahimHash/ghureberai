@extends('ghurboMama.layouts.master')

@section('content')

	<div class="container">
	    <div class="row">
	    	
	        <div class="col-md-8 col-md-offset-2">
	        	<h3>Divisions</h3>
            <p style="color: #bfbfbf;margin-bottom: 50px">Visitor site access stat by <i>divisions</i></p>
            
	          <div id="divContainer" data-divisions="{{$divisions}}"></div>
	        	<div id="columnchart_material" style="width: 100%; height: 70%;"></div>

	        	
	        </div>
	    </div>
	</div>

@endsection

@section('script')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      var arrayFromElement = $( "#divContainer" ).data( "divisions" );
      var tableData = [['Division', 'Hits']];

      for (var i = 0; i < arrayFromElement.length; i++) {
      	tableData.push([arrayFromElement[i].division,arrayFromElement[i].total]);
      }

      console.log(tableData);
      function drawChart() {
        var data = google.visualization.arrayToDataTable(tableData);

        var options = {
          chart: {
            // title: 'Visitor Division Data',
            // subtitle: 'Location Searches From Divisions',

          }
          
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>

@endsection

