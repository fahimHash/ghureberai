@extends('ghurboMama.layouts.master')

@section('content')

	<div class="container">
	    <div class="row">
	    	
	        <div class="col-md-8 col-md-offset-2">
	        	<h3>{{$division}}</h3>
            <p style="color: #bfbfbf">Visitors site access data based on districts in <i>{{$division}}</i> division</p>
            <hr>
	          <div id="areaContainer" data-areas="{{$areas}}"></div>
	        	<div id="piechart" style="width: 900px; height: 500px;"></div>

	        	
	        </div>
	    </div>
	</div>

@endsection

@section('script')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      var arrayFromElement = $( "#areaContainer" ).data( "areas" );
      var tableData = [['District', 'Hits']];


      for (var i = 0; i < arrayFromElement.length; i++) {
        tableData.push([arrayFromElement[i][0],arrayFromElement[i][1]]);
      }

      console.log(arrayFromElement);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(tableData);

        var options = {
          // title: 'District Hits Density'
         
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

@endsection

