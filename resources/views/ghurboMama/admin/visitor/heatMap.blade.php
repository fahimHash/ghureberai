@extends('ghurboMama.layouts.master')

@section('content')

	<div class="container">
	    <div class="row">
	    	
	        <div class="col-md-12">
	        	<div id="mapData" data-mapData="{{$mapData}}"></div>
	            {{-- map --}}
		    	<div id="heatMap" style="height: 100%"></div>
		    	{{-- map --}}
	        </div>
	    </div>
	</div>

@endsection

@section('script')

<script type="text/javascript" src="{{ URL::asset('/js/visitorHeatMap.js') }}" defer></script>

@endsection

