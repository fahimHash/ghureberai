@extends('ghurboMama.layouts.master')

@section('content')

	<div class="container">
	    <div class="row">
	    	
	        <div class="col-md-12">
	        	<h3>Users</h3>
	        	<hr>
	            <table class="table table-hover table-bordered">
				 	<tr> 
				 		<th>#</th>
				 		<th>Username</th> 
				 		<th>First Name</th> 
				 		<th>Last Name</th> 
				 		<th>Email</th> 
				 		<th>Address</th> 
				 		<th>Budget</th> 
				 	</tr>

				 	@foreach($users as $user)
				 		<tr>
				 			<td><h5 style="color:#292b2c">{{$user->id}}</h5></td>
				 			<td><h5 style="color:#428bca">{{$user->username}}</h5></td>
				 			<td><h5 style="color:#428bca">{{$user->first_name}}</h5></td>
				 			<td><h5 style="color:#428bca">{{$user->last_name}}</h5></td>
				 			<td><h5 style="color:#428bca">{{$user->email}}</h5></td>
				 			<td><h5 style="color:#428bca">{{$user->address}}</h5></td>
				 			<td><h5 style="color:#428bca">{{$user->budget}}/-</h5></td>
				 		</tr>
				 	@endforeach
				</table>
	        </div>

	    </div>
	</div>

@endsection

