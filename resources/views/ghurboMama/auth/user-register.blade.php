@extends('ghurboMama.layouts.master')


@section('content')


<div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                  <div class="jumbotron">
                    <div class="row">
                      <div class="col-md-12">
                        <h2>Register</h2>
                      </div> 
                    </div>
                    <br> 
                    <form method="post" role="form" action="{{route('register')}}">
                      <div class="form-group{{$errors->has('first_name') ? ' has-error' : ''}}">
                        <label for="">First Name</label>
                        <input type="text" class="form-control" id="" placeholder="First Name" name="first_name">
                        @if($errors->has('first_name'))
                          <span class="help-block">{{$errors->first('first_name')}}</span>
                        @endif  
                      </div>
                      <div class="form-group{{$errors->has('last_name') ? ' has-error' : ''}}">
                        <label for="">Last Name</label>
                        <input type="text" class="form-control" id="" placeholder="Last Name" name="last_name">
                        @if($errors->has('last_name'))
                          <span class="help-block">{{$errors->first('last_name')}}</span>
                        @endif
                      </div>


                      <div class="form-group{{$errors->has('default_budget') ? ' has-error' : ''}}">
                        <label for="">An average budget You wish to spend on tours</label>
                        <input type="text" class="form-control" id="" placeholder="5000" name="default_budget">
                        @if($errors->has('default_budget'))
                          <span class="help-block">{{$errors->first('default_budget')}}</span>
                        @endif
                      </div>

                      <div class="form-group{{$errors->has('email') ? ' has-error' : ''}}">
                        <label for="">Email address</label>
                        <input type="email" class="form-control" id="" placeholder="Email"
                        name="email">
                        @if($errors->has('email'))
                          <span class="help-block">{{$errors->first('email')}}</span>
                        @endif
                      </div>
                      <div class="form-group{{$errors->first('password') ? ' has-error' : ''}}">
                        <label for="">Password</label>
                        <input type="password" class="form-control" id="" placeholder="Password" name="password">
                        @if($errors->has('password'))
                          <span class="help-block">{{$errors->first('password')}}</span>
                        @endif
                      </div>

                      {{ csrf_field() }}

                      <button type="submit" class="btn btn-default">Register</button>

                      </form>
                    </div>
                </div>
            </div>
        </div>
</div>


@endsection