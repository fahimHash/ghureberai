
      
@foreach(Session::get('collSuggest')->getCollection() as $k => $c)

    <?php  $count = $k; $count=$count+1; ?>
    <tr>
        <td>{{$count}}</td>
        <td class="cus-table-location-name-details">
            <a href="#">
                <img src="{{URL::asset('/location_pictures')}}/{{$c->location_pic}}" height="128" width="128">
            </a>
            <h4><a href="{{URL::to('/')}}/location/{{$c->location_name}}">{{ $c->location_name }} {{ $c->location_name_bangla }}</a></h4>
            <p>
                <strong>Division:</strong> <a href="{{route('searchByDivision',
                                  ['division' => $c->division->division_name])}}">{{ $c->division->division_name }}</a><br>

                <strong>District:</strong> <a href="{{route('searchByDistrict',
                                  ['district' => $c->district->district_name])}}">{{ $c->district->district_name }}</a><br>

                <strong>Upazila:</strong> <a href="{{route('searchByUpazila',
                                  ['upazila' => $c->upazila->upazila_name])}}">{{ $c->upazila->upazila_name }}</a><br>

                <strong>Visited:</strong> <a href="#">{{ 
                                        $c->visited 
                                        }} </a><br>

                <strong>Distance:</strong> <a href="#">{{ 
                                round($c->distance,1) 
                                }} KM</a><br>    
                                                    
                <span id="loclat" style="visibility:hidden" data-loclat={{$c->latitude}}></span>
                <span id="loclon" style="visibility:hidden" data-loclon={{$c->longitude}}></span>

                <a href="#gmap" class="btn btn-sm btn-default vmap" rel="modal:open">
                    <i class="fa fa-map-marker fa-lg" ></i> View Map
                </a>



            </p>
        </td>
        <td><a href="#">{{ $c->location_type }}</a></td>
        <td class="text-center cus-table-transport">
                                <!-- Bus, Plane -->
            <i class="fa fa-bus fa-lg active"></i>
            <i class="fa fa-train fa-lg"></i>
            <i class="fa fa-ship fa-lg"></i>
            <i class="fa fa-plane fa-lg"></i>
        </td>
        <td class="cus-table-cost">
            <span class="cus-cost"><i class="fa fa-money fa-lg"></i> 
                                {{($c->cost_transport)+($c->cost_accommodation)}}/-</span>
            <hr>
            <em>Living: {{$c->cost_accommodation}}/- (Per Night)<br>
                Transport: {{$c->cost_transport}}/-</em>
        </td>
        <td class="cus-table-ratting">
            <div id="hearts-existing" class="starrr" data-rating="{{$c->rating}}"></div>
        </td>
        <td class="text-center">
            <a href="{{URL::to('/')}}/location/{{$c->location_name}}" class="btn btn-sm btn-success">View Details</a>
        </td>
        
    </tr>
@endforeach  
