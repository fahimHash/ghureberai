    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
                <a class="navbar-brand" href="{{route('home')}}"><span><i class="fa fa-themeisle fa-lg"></i> GhureBerai</span></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="{{route('home')}}">Home</a>
                    </li>
                    @if(Auth::check())
                        <li>
                            <a href="{{route('UserProfile')}}">My Profile</a>
                        </li>
                    @endif
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Contacts</a>
                    </li>
                    @if(Auth::check())
                        <li>
                            <a class="btn-register" href="{{route('logout')}}">Logout</a>
                        </li>
                    @else    
                        <li>
                            <a class="btn-register" href="{{route('register')}}">Register</a>
                        </li>
                        <li>
                            <a class="btn-login" href="{{route('login')}}">Login</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>