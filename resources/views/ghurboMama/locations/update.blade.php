@extends('ghurboMama.layouts.master')



@section('headScripts')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({ selector:'textarea' });
    </script> 
@endsection


@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="panel panel-default">
					<div class="panel-heading">Dashboard - Update locations</div>
					<div class="panel-body">
						@foreach($locations as $l)
							<ul class="list-group">
								<li class="list-group-item clearfix">

									<span>{{$l->location_name}}</span>
									
									<span class="pull-right" style="padding-left: 10px;">
										<a class="btn btn-danger delete">Delete</a>
									</span>
									<span class="pull-right" style="padding-left: 10px;">
										<a class="btn btn-primary edit" data-id="{{$l->id}}">Edit</a>
									</span>

								</li>	
							</ul>
						@endforeach

						{{$locations->links()}}
					</div>
				</div>
			</div>

			<div class="col-md-5">
			<div class="panel panel-default">
			<div class="panel-heading">Update</div>

			<div class="panel-body">
				<form class="form" method="post" enctype="multipart/form-data" action="{{route('editLocation')}}">

					<div class="form-group{{$errors->has('form-en-name') ? ' has-error' : ''}}">
					    <label for="lname-en">Location Name</label>
					    <input type="text" class="form-control" id="lname-en" placeholder="" name="form-en-name">
					</div>

					<div class="form-group{{$errors->has('form-bn-name') ? ' has-error' : ''}}">
					    <label for="lname-bn">Location Name Bangla</label>
					    <input type="text" class="form-control" id="lname-bn" placeholder="" name="form-bn-name">
					</div>

	                <div class="form-group{{$errors->has('form-loc-img') ? ' has-error' : ''}}">

	                    <label for="form-img">Upload Location Image</label>   
	                    <input class="btn btn-success" type="file" id="form-img" name="form-loc-img">
	                     
	                </div>

	                <div class="form-group{{$errors->has('form-loc-img-gallery') ? ' has-error' : ''}}">
	                
                     <label for="form-img-gallery">Upload Images for Gallery</label>
                     
                     <input class="btn btn-info" type="file" id="form-img-gallery" name="form-loc-img-gallery[]" multiple>
                     
                   </div>

	          		<div class="form-group{{$errors->has('form-cost-trans') ? ' has-error' : ''}}">

					    <label for="tcost">Transport Cost</label>
					    <input type="number" class="form-control" id="tcost"
					    name="form-cost-trans" placeholder="1000">

					</div>

					<div class="form-group{{$errors->has('form-cost-accom') ? ' has-error' : ''}}">

					    <label for="acost">Acommodation Cost</label>
					    <input type="number" class="form-control" id="acost"
					    name="form-cost-accom" placeholder="1000" >

					</div>

					<div class="form-group{{$errors->has('form-lat') ? ' has-error' : ''}}">

					    <label for="flat">Latitude</label>
					    <input type="number" class="form-control" id="flat" placeholder="90.654" name="form-lat" step="0.0000001">

					</div>

					<div class="form-group{{$errors->has('form-lon') ? ' has-error' : ''}}">

					    <label for="flon">Longitude</label>
					    <input type="number" class="form-control" id="flon" placeholder="90.654" name="form-lon" step="0.0000001">

					</div>

					<div class="form-group{{$errors->has('form-description') ? ' has-error' : ''}}">

					    <label for="fdes">Location Description</label>
					    <textarea type="text" name="form-description" id="fdes" class="form-control" rows="7" placeholder="Some common information about this place..."></textarea>

					</div>

					 <input type="hidden" value="" name="id" id="id">

					 <button type="submit" class="btn btn-block btn-success" name="form-submit">Update Location</button>
					{{ csrf_field() }}

				</form>

			</div>	
				
			</div>	
			</div>	
			</div>
		</div>
	</div>	

@endsection

@section('script')

<script type="text/javascript">

	$(document).ready(function(){

		$('.edit').click(function(){
			
			$.ajax({
				type : 'GET',
				data : {id : $(this).attr('data-id')},
				dataType : 'json',
				url : "{{route('getLocationDataForEdit')}}"
			})
			.done(function(data){
				console.log(data);

				$('#lname-en').attr('value',data[0].location_name);
				$('#lname-bn').attr('value',data[0].location_name_bangla);
				

				$('#tcost').val(data[0].cost_transport);
				$('#acost').val(data[0].cost_accommodation);

				$('#flat').val(data[0].latitude);
				$('#flon').val(data[0].longitude);

				// $('#fdes').val(data[0].location_info);
				// tinymce.setContent(data[0].location_info);
				tinymce.editors[0].setContent(data[0].location_info);
				$('#id').val(data[0].id);

			}
			);
		}
		);

	}
	);
</script>

@endsection