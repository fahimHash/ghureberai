@extends('ghurboMama.layouts.master')



@section('headScripts')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({ selector:'textarea' });
    </script> 
@endsection

@section('content')

	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Dashboard - Add a location</div>

	                <div class="panel-body">
	                    <h3>Location</h3>
	                    <hr>
	                    
	                    <form class="form" method="post" action="{{route('addLocation')}}"
	                    enctype="multipart/form-data">

						  <div class="form-group{{$errors->has('form-en-name') ? ' has-error' : ''}}">
						    <label for="lname-en">Location Name</label>
						    <input type="text" class="form-control" id="lname-en" placeholder="Location Name in english" name="form-en-name" value="{{Request::old('form-en-name') ?: '' }}">
						    
                            @if($errors->has('form-en-name'))
                                <span class="help-block">{{$errors->first('form-en-name')}}</span>
                            @endif
						  </div>

						  <div class="form-group{{$errors->has('form-bn-name') ? ' has-error' : ''}}">
						    <label for="lname-bn">Location Name Bangla</label>
						    <input type="text" class="form-control" id="lname-bn" placeholder="Location Name in bengali" name="form-bn-name" value="{{Request::old('form-bn-name') ?: '' }}">

                            @if($errors->has('form-bn-name'))
                                <span class="help-block">{{$errors->first('form-bn-name')}}</span>
                            @endif
						  </div>

						  <div class="form-group{{$errors->has('form-div') ? ' has-error' : ''}}">
						  	<label for="form-divisions">Division</label>
                            <select class="form-control" id="form-divisions" name="form-div" value="{{Request::old('form-div') ?: '' }}">
                            	<option>Division</option>
                                <option>Dhaka</option>
                                <option>Rajshahi</option>
                                <option>Rangpur</option>
                                <option>Barisal</option>
                                <option>Sylhet</option>
                                <option>Khulna</option>
                                <option>Chittagong</option>
                            </select>

                            @if($errors->has('form-div'))
                                <span class="help-block">{{$errors->first('form-div')}}</span>
                            @endif
                          </div>

                          <div class="form-group{{$errors->has('form-dis') ? ' has-error' : ''}}">
                          	  <label for="form-districts">District</label>
	                          <select class="form-control" id="form-districts" name="form-dis" value="{{Request::old('form-dis') ?: '' }}">
	                                <option>District</option>
	                          </select>

                              @if($errors->has('form-dis'))
                                <span class="help-block">{{$errors->first('form-dis')}}</span>
                              @endif
	                       </div>


	                       <div class="form-group{{$errors->has('form-upz') ? ' has-error' : ''}}">
	                         <label for="form-upazila">Upazila</label>
	                         <select class="form-control" id="form-upazila" name="form-upz" value="{{Request::old('form-upz') ?: '' }}">
	                           <option>Upazila</option>
	                         </select>

                              @if($errors->has('form-upz'))
                                <span class="help-block">{{$errors->first('form-upz')}}</span>
                              @endif
	                       </div>

	                       <div style="display: inline-block; margin-right: 15px;" class="form-group{{$errors->has('form-loc-img') ? ' has-error' : ''}}">
	                         <label for="form-img">Upload Location Image</label>
	                         <input class="btn btn-primary" type="file" id="form-img" name="form-loc-img" value="{{Request::old('form-loc-img') ?: '' }}">

                              @if($errors->has('form-loc-img'))
                                <span class="help-block">{{$errors->first('form-loc-img')}}</span>
                              @endif
	                         
	                       </div>

                           <div style="display: inline-block;margin-left: 15px;" class="form-group{{$errors->has('form-loc-img-gallery') ? ' has-error' : ''}}">
                             <label for="form-img-gallery">Upload Images for Gallery</label>
                             
                             <input class="btn btn-success" type="file" id="form-img-gallery" name="form-loc-img-gallery[]" multiple value="{{Request::old('form-loc-img-gallery') ?: '' }}">
                             
                              @if($errors->has('form-loc-img-gallery'))
                                <span class="help-block">{{$errors->first('form-loc-img-gallery')}}</span>
                              @endif
                           </div>

	                       <div class="form-group{{$errors->has('form-cost-trans') ? ' has-error' : ''}}">
						    <label for="tcost">Transport Cost</label>
						    <input type="number" class="form-control" id="tcost"
						    name="form-cost-trans" placeholder="1000" value="{{Request::old('form-cost-trans') ?: '' }}">

                            @if($errors->has('form-cost-trans'))
                                <span class="help-block">{{$errors->first('form-cost-trans')}}</span>
                            @endif
						  </div>

						  <div class="form-group{{$errors->has('form-cost-accom') ? ' has-error' : ''}}">
						    <label for="acost">Acommodation Cost</label>
						    <input type="number" class="form-control" id="acost"
						    name="form-cost-accom" placeholder="1000" value="{{Request::old('form-cost-accom') ?: '' }}">

                            @if($errors->has('form-cost-accom'))
                                <span class="help-block">{{$errors->first('form-cost-accom')}}</span>
                            @endif
						  </div>

						  
						  <div class="form-group{{$errors->has('form-lat') ? ' has-error' : ''}}">
						    <label for="flat">Latitude</label>
						    <input type="number" class="form-control" id="flat" placeholder="23.432" name="form-lat" step="0.0000001" value="{{Request::old('form-lat') ?: '' }}">

                            @if($errors->has('form-lat'))
                                <span class="help-block">{{$errors->first('form-lat')}}</span>
                            @endif
						  </div>

						  <div class="form-group{{$errors->has('form-lon') ? ' has-error' : ''}}">
						    <label for="flon">Longitude</label>
						    <input type="number" class="form-control" id="flon" placeholder="90.654" name="form-lon" step="0.0000001" value="{{Request::old('form-lon') ?: '' }}">

                            @if($errors->has('form-lon'))
                                <span class="help-block">{{$errors->first('form-lon')}}</span>
                            @endif
						  </div>

						  <div class="form-group{{$errors->has('form-description') ? ' has-error' : ''}}">
						    <label for="fdes">Location Description</label>
						    <textarea type="text" name="form-description" id="fdes" class="form-control" rows="7" placeholder="Some common information about this place..." value="{{Request::old('form-description') ?: '' }}"></textarea>

                            @if($errors->has('form-description'))
                                <span class="help-block">{{$errors->first('form-description')}}</span>
                            @endif
						  </div>

                          
						  <button type="submit" class="btn btn-block btn-success" name="form-submit">Add Location</button>
						  {{ csrf_field() }}
						</form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection


@section('script')

<script type="text/javascript">
    
    $(document).ready(function()
    {

       

        var disBydiv =  <?php if(isset($disBydiv)){ echo $disBydiv;  }?> 
        var upBydis  =  <?php if(isset($upBydis)){ echo $upBydis;  }?>

      

        $("#form-divisions").click(function()
        {

            var x = $("#form-divisions option:selected")[0].innerText;

            if(x == "Division")
            {
                $("#form-districts")
                    .empty()
                    .append($("<option>District</option>"));

                $("#form-upazila")
                    .empty()
                    .append($("<option>Upazila</option>"));
            }else
            {
                $("#form-districts").empty();

                $("#form-districts")
                    .empty()
                    .append($("<option>District</option>"));

                $("#form-upazila")
                    .empty()
                    .append($("<option>Upazila</option>"));

                disBydiv.forEach(function(item) {

                    if(item.division == x)
                    {   
                        var el = $("<option>"+item.district+"</option>");
                        $("#form-districts").append(el);
                    }


                });                        
            }

        });


        $("#form-districts").click(function()
        {

            var x = $("#form-districts option:selected")[0].innerText;
            
            if(x == "District")
            {
                $("#form-upazila")
                    .empty()
                    .append($("<option>Upazila</option>"));
            }else
            {
                $("#form-upazila").empty();
                $("#form-upazila")
                    .empty()
                    .append($("<option>Upazila</option>"));

                upBydis.forEach(function(item)
                {

                    if(item.district == x)
                    {   
                        var el = $("<option>"+item.upazila+"</option>");
                        $("#form-upazila").append(el);
                    }

                });                        
            }

        });              

    }
    ); 

</script>	

@endsection