@extends('ghurboMama.layouts.master')

@section('content')


<div id="gmap" style="display: none"></div>
<div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron text-center">
                      <h1>
                      	{{$location->location_name}}
                      	{{$location->location_name_bangla}}
                      </h1>

                      @if($location->location_pic == 'Not Found')
                      <img src="https://unsplash.imgix.net/photo-1422022098106-b3a9edc463af?w=1024&amp;q=50&amp;fm=jpg&amp;s=bbe0beb19ba846e0de37abd1f63acf13" width="600" height="300" />
                      @else

                      <img src="{{URL::asset('/location_pictures')}}/{{$location->location_pic}}" width="600" height="300" />


                      @endif

                      <table class="location-details">
                      <tr>
                          <td width="200">

                                  <strong>Division:</strong>
                                  <a href="{{route('searchByDivision',
                                  ['division' => $location->division->division_name])}}">
                                  	{{$location->division->division_name}}
                                  </a>
                                  <br>

                                  <strong>District:</strong>
                                  <a href="{{route('searchByDistrict',
                                  ['district' => $location->district->district_name])}}">
                                  	{{$location->district->district_name}}
                                  </a>
                                  <br>

                                  <strong>Upazila:</strong>
                                  <a href="{{route('searchByUpazila',
                                  ['upazila' => $location->upazila->upazila_name])}}">
                                  	{{$location->upazila->upazila_name}}
                                  </a>
                                  <br>

                                  <strong>Visited:</strong>
                                  <a href="#">
                                    {{$location->visited}}
                                  </a>
                                  <br>

                                  <span id="lat" style="display: none" data-lat="{{$location->latitude}}"></span>

                                  <span id="lon" style="display: none" data-lon="{{$location->longitude}}"></span>

                                  <a href="#gmap" class="btn btn-sm btn-default vmap" rel="modal:open">
                                      <i class="fa fa-map-marker fa-lg" ></i> View Map
                                  </a>
                                  
                                  

                          </td>
                          <td width="120"><strong>Location Type:</strong> <br><a href="#">River</a></td>
                          <td width="200" class="text-center cus-table-transport">
                            <strong>Transport:</strong><br>
                              <!-- Bus, Plane -->
                              <i class="fa fa-bus fa-lg active"></i>
                              <i class="fa fa-train fa-lg"></i>
                              <i class="fa fa-ship fa-lg"></i>
                              <i class="fa fa-plane fa-lg"></i>
                          </td>
                          <td width="200" class="cus-table-cost">
                            <strong>Cost</strong><br>
                              <span class="cus-cost"><i class="fa fa-money fa-lg"></i>
                              {{$location->cost_accommodation}}/-</span>
                              <hr>
                              <em>Living: 1500/- (Per Night)<br>
              Transport: 2000/-</em>
                          </td>
                          <td class="cus-table-ratting">
                            <strong>Rating</strong><br>
                              <div id="hearts-existing" class="starrr location_rating" data-rating="{{$location->rating}}"></div>
                          </td>

                          @if(Auth::check())
                          <td class="cus-table-ratting">
                            <strong>Rate This Spot</strong><br>
                              <div id="user-rated" class="starrr user-rated" data-rating="0"></div>
                          </td>
                          @endif
                          {{-- toChange --}}
                          <td>
                            <strong>Share On Facebook</strong><br>
                              <a class="btn btn-default" style="padding-left: 25px; padding-right: 25px; padding-top: 5px; padding-bottom: 5px;  " href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode('https://developers.google.com/chart/interactive/docs/gallery') }}" target='_blank'><i class="fa fa-fw fa-facebook text-inverse"></i></a>
                          </td>  
                      </tr>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if(!empty($images))

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3><span style="color: #00b22d">{{$location->location_name}}</span> Images</h3>
            <hr>
            <div class="gallery-box" style="display:flex;flex-direction:row; overflow: scroll;padding: 10px solid #f2f2f2;background-color: #f2f2f2">
              @foreach($images as $img)
                <a href="{{$img}}" target="blank"><img src="{{$img}}" style="height: 300px; width: auto;margin-right: 5px "></a>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>


    <br>

    @endif

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3>About <span style="color: #00b22d">{{$location->location_name}}</span></h3>
                    <hr>
                    <div class="location-content" style="margin-bottom: 40px">{!!$location->location_info!!}</div>

                    <div style="clear: both"></div>

                    <h3>Accommodation</h3>
                    
                    <table class="table">
                      <thead>
                        <tr>
                          <td></td>
                          <td><strong>Name</strong></td>
                          <td><strong>Address</strong></td>
                          <td><strong>Phone</strong></td>
                          <td><strong>Cost</strong></td>
                        </tr>
                      </thead>
                      <tbody>

                        <?php $count_hotels=1; ?>
                        
                        @foreach($hotels as $h)
                        <tr>
                          <td>{{$count_hotels}}</td>
                          <td>{{$h->hotel_name}}</td>
                          <td>{{$h->address}}</td>
                          <td>{{$h->contact}}</td>
                          <td>{{$h->cost}}</td>
                        </tr>
                        <?php $count_hotels++; ?>
                        @endforeach
                      </tbody>
                    </table>

                </div>
                <div class="col-md-4">
                  <h4>Nearest places in <span style="color: #00b22d">{{str_replace("_", " ", $location->upazila->upazila_name)}}</span>
                    <span style="display: block; font-style: italic;">waiting for you</span></h4>
                  <table class="table">
                  <?php $count=1; ?>	
                  @foreach($nearest as $n)

                    <tr>
                        <td>{{$count}}</td>
                        <td class="cus-table-location-name-details">
                            <a href="#">
                                <img src="{{URL::asset('/location_pictures')}}/{{$n->location_pic}}" height="32" width="32">
                            </a>
                            <h4>
                            	<a href="{{URL::to('/')}}/location/{{$n->location_name}}">
                            		{{$n->location_name}} 
                            		{{$n->location_name_bangla}} 
                            	</a>
                            </h4>

                        </td>
                        <td class="cus-table-ratting">
                            <div id="hearts-existing" class="starrr" data-rating="{{$n->rating}}"></div>
                        </td>
                    </tr>
                    <?php $count++; ?>
                   @endforeach 
           
                  </table>
                </div>
            </div>
        </div>
 </div>



@endsection



@section('script')
  
  <script type="text/javascript" src="{{ URL::asset('/js/viewMapInDetailsPage.js') }}"></script>

  @if(Auth::check())
  <script type="text/javascript">

      $(document).ready(function(){


              var userRating = document.getElementById('user-rated').getAttribute("data-rating");
              var userRatingOnLeave = userRating;

              var user= {{Auth::user()->id}};
              var location = {{$location->id}}
              console.log(location);


              $('#user-rated').on('starrr:change', function(e, value) {
                  this.setAttribute("data-rating",value);
                  userRatingOnLeave = this.getAttribute("data-rating");

                  if (userRatingOnLeave != 'undefined'){
                    $.ajax({
                        type: 'GET',
                        url: '{{route("submitRating")}}',
                        data: {userRatingOnLeave : userRatingOnLeave,
                               user              : user,
                               location          : location
                              },
                        success : function(msg){
                          console.log(msg);
                        }
                    });
                  }

                  
                  
              });





      });

  </script>
  @endif
@endsection