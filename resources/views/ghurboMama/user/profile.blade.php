@extends('ghurboMama.layouts.master')

@section('content')

	<div class="section">
		<div class="container">
			<div class="row">

				<div class="col-md-6" >
					<h1>{{ucfirst($user->first_name)}} {{ucfirst($user->last_name)}}</h1>
					<p><span>E Mail :</span><i> {{$user->email}}</i></p>
					<p>Address : {{$user->address}}</p>
					<p>Budget You Specified : {{$user->budget}}</p>
					<hr>
				</div>
							
			</div>

			<div class="row">
				<div class="col-md-12">
					<h3>Places you seem to like</h3>
					<hr>
					

						
		                  
		                  @foreach($ratings as $n)

		                    	<div style="display: inline-block; margin-right: 30px;margin-bottom: 20px;">
		                        
		                            <a href="#">
		                                <img src="{{URL::asset('/location_pictures')}}/{{$n->location->location_pic}}" height="32" width="32">
		                            </a>
		                            <h4>
		                            	<a href="/location/{{$n->location->location_name}}">
		                            		<div>{{$n->location->location_name}}</div> 
		                            		<div>{{$n->location->location_name_bangla}}</div> 
		                            	</a>
		                            </h4>

		                            <div id="hearts-existing" class="starrr" data-rating="{{$n->location->rating}}"></div>
		                       
		                    	</div>
		                    
		                   @endforeach 
		           
		               

				

				</div>	
			</div>
		</div>
	</div>

@endsection