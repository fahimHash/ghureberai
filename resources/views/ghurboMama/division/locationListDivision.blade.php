@extends('ghurboMama.layouts.master')

@section('content')

<div id="gmap" style="display: none"></div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 style="text-align: center;">List of Locations in <span style="color: #00B22D">{{$division}}</span> Division</h2>
			<hr>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
		    <table class="table">
		        <thead>
		            <tr>
		                <th>#</th>
		                <th>Location Name</th>
		                <th>Type</th>
		                <th class="text-center">Transport</th>
		                <th>Trip Cost (BDT)</th>
		                <th>Rating</th>
		                <th class="text-center">Hotel/Rest House</th>
		            </tr>
		        </thead>
		        <tbody>
		        	@foreach($locations->getCollection() as $k => $c)

					    <?php  $count = $k; $count=$count+1; ?>
					    <tr>
					        <td>{{$count}}</td>
					        <td class="cus-table-location-name-details">
					            <a href="#">
					                <img src="{{URL::asset('/location_pictures')}}/{{$c->location_pic}}" height="128" width="128">
					            </a>
					            <h4><a href="{{URL::to('/')}}/location/{{$c->location_name}}">{{ $c->location_name }} {{ $c->location_name_bangla }}</a></h4>
					            <p>
					                <strong>Division:</strong> <a href="{{route('searchByDivision',['division' => $c->division->division_name])}}">{{ $c->division->division_name }}</a><br>

					                <strong>District:</strong> <a href="{{route('searchByDistrict',['district' => $c->district->district_name])}}">{{ $c->district->district_name }}</a><br>

					                <strong>Upazila:</strong> <a href="{{route('searchByUpazila',['upazila' => $c->upazila->upazila_name])}}">{{ $c->upazila->upazila_name }}</a><br>

					                <strong>Visited:</strong> <a href="#">{{ 
					                                        $c->visited 
					                                        }} </a><br>

					                
					                                                    
					                <span id="loclat" class="loclat" style="visibility:hidden" data-loclat={{$c->latitude}}></span>
					                <span id="loclon" class="loclon" style="visibility:hidden" data-loclon={{$c->longitude}}></span>

					                <a href="#gmap" class="btn btn-sm btn-default vmap" rel="modal:open">
					                    <i class="fa fa-map-marker fa-lg" ></i> View Map
					                </a>



					            </p>
					        </td>
					        <td><a href="#">{{ $c->location_type }}</a></td>
					        <td class="text-center cus-table-transport">
					                                <!-- Bus, Plane -->
					            <i class="fa fa-bus fa-lg active"></i>
					            <i class="fa fa-train fa-lg"></i>
					            <i class="fa fa-ship fa-lg"></i>
					            <i class="fa fa-plane fa-lg"></i>
					        </td>
					        <td class="cus-table-cost">
					            <span class="cus-cost"><i class="fa fa-money fa-lg"></i> 
					                                {{($c->cost_transport)+($c->cost_accommodation)}}/-</span>
					            <hr>
					            <em>Living: {{$c->cost_accommodation}}/- (Per Night)<br>
					                Transport: {{$c->cost_transport}}/-</em>
					        </td>
					        <td class="cus-table-ratting">
					            <div id="hearts-existing" class="starrr" data-rating="{{$c->rating}}"></div>
					        </td>
					        <td class="text-center">
					            <a href="{{URL::to('/')}}/location/{{$c->location_name}}" class="btn btn-sm btn-success">View Details</a>
					        </td>
					        
					    </tr>
					@endforeach
		        </tbody>

		    </table>

		    <hr>
		</div>
	</div>


	<div class="section">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 text-center">

	                    {{$locations->links()}}   

	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection

@section('script')
<script type="text/javascript" src="{{ URL::asset('/js/viewMap.js') }}"></script>
@endsection