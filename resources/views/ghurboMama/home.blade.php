@extends('ghurboMama.layouts.master')


@section('content')
	 <div class="section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div style="text-align: center; min-height: 500px;background: url('{{URL::asset('/images/bd07.jpg')}}');background-size: cover;background-position:center;background-repeat: no-repeat; margin-bottom: 30px;padding-top: 100px" id="home_jumbo">
                        
                        <h1 style="color: #fff;text-align: center;">Hello,
                          @if(Auth::check())
                            {{ucfirst(Auth::user()->first_name)}}
                          @else     
                            Traveller!
                          @endif  
                        </h1>
                        <p style="color: #fff">You won't miss a thing! We listed all! All over Bangladesh!</p>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3" >
            <form role="form" action="{{route('locationsUserSearchedForInBd')}}" method="get" id="bdSearch">
                                    <div class="form-group form-group-lg">
                                        <div class="input-group">
                                            <input style="border-radius: 0px;background-color: transparent;border-color: #fff;color: #fff;" type="text" class="form-control" placeholder="Search Bangladesh..." name="form-searchBd"> 

                                            <div class="input-group-btn"> 
                                                <button style="border-radius: 0px" type="submit" class="btn btn-lg btn-success" name="form-submit">Explore</button> 
                                            </div>
                                             <input type="hidden" name="lat" value="">
                                             <input type="hidden" name="lon" value=""> 
                                        </div>
                                    </div>
                                    {{ csrf_field() }}
            </form>
                            </div>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="visitor_map" style="width: 100%;min-height: 500px;">
                            
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-filter">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
    
                        <div id="weather"></div>
                        <h3>
                            <i class="fa fa-map-marker"></i> Your Location :
                            <span id="location">
                                <span id="wait">Please Wait....</span>
                            <span id="div"></span>
                        </h3>
                    
                        <form id="nearbyForm" role="form" method="get" class="form-inline text-center" action="{{ route('locationsNearby') }}" >

                                <input type="hidden" name="div" value="Norway">
                                <input type="hidden" name="dis" value="Norway">
                                <input type="hidden" name="lat" value="Norway">
                                <input type="hidden" name="lon" value="Norway">

                                @if(Auth::check())
                                    <button type="submit" class="btn btn-success" name="form-submit-suggest">
                                        Suggest Me
                                    </button>

                                    <button type="submit" class="btn btn-success" name="form-submit-nearby">
                                        Search Nearby
                                    </button>
                                @else
                                    <button type="submit" class="btn btn-success" name="form-submit-nearby">
                                        Search Nearby
                                    </button>
                                @endif

                                {{ csrf_field() }}
                        </form>
                    



                </div>
                <div class="col-md-12">
                    <form role="form" method="get" class="form-inline text-center" action="{{ route('locationsUserSearchedFor') }}" id="searchForm">
                        <div class="form-group">
                            <div class="button-slider">
                                <div class="btn-group">
                                    <div class="btn btn-default">Budget</div>
                                    <div class="btn btn-default">
                                        <input id="bootstrap-slider" type="text" data-slider-min="500" data-slider-max="50000" data-slider-step="500" data-slider-value="500" name="form-budget" />
                                        <div class="valueLabel">From BDT <span id="sliderValue">500</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="form-control" id="form-divisions" name="form-div">
                                <option>Division</option>
                                <option>Dhaka</option>
                                <option>Rajshahi</option>
                                <option>Rangpur</option>
                                <option>Barisal</option>
                                <option>Sylhet</option>
                                <option>Khulna</option>
                                <option>Chittagong</option>
                            </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-districts" name="form-dis">
                                <option>District</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-upazila" name="form-upz">
                            <option>Upazila</option>
                          </select>
                        </div>
                        {{-- <div class="form-group">
                            <select class="form-control" name="form-type" value="">
                        <option>Type</option>
                        <option>River</option>
                        <option>Mazar</option>
                        <option>Hill</option>
                        <option>Historical</option>
                      </select>
                        </div> --}}
                        

                        <input type="hidden" name="dis" value="">
                        <input type="hidden" name="lat" value="">
                        <input type="hidden" name="lon" value="">

                        <button type="submit" class="btn btn-success" name="form-submit">Search</button>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="section">
        <div class="container">


            <div id="gmap" style="display: none"></div>
          

            @if(Session::has('collNearBy'))
                <div class="row"  style="margin-bottom: 25px;">
                    <div class="col-md-6 col-md-offset-3">
                        <a href="#gmap" id="showAllMap" class="btn btn-success btn-block" rel="modal:open">See All in Map</a>
                    </div>
                </div>
            @endif

            <div class="row">
                @if(empty(Session::has('coll'))        && 
                    empty(Session::has('collBd'))      &&
                    empty(Session::has('collNearBy'))  &&
                    empty(Session::has('collSuggest')) 
                   )
                    <div class="col-md-12">
                        <div class="panel panel-success" style="border-radius: 0px">
                            <div class="panel-body" style="background-color: #f9f9f9;">
                                <h4 style="text-align: center;">
                                    @if(Auth::check())
                                        So 
                                        <span style="color: #4cae4c">
                                            {{ucfirst(Auth::user()->first_name)}}</span>,
                                    @endif

                                    For you we listed below Some places other people seems to be interested in. Take a look...
                                </h4>
                            </div>
                        </div>        
                        
                        <br>
                    </div>
                @endif
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Location Name</th>
                                <th>Type</th>
                                <th class="text-center">Transport</th>
                                <th>Trip Cost (BDT)</th>
                                <th>Rating</th>
                                <th class="text-center">Location Datails</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(Session::has('coll'))

                            @include('ghurboMama.partials.locationTables.locationTable')

                        @elseif(Session::has('collBd'))

                            @include('ghurboMama.partials.locationTables.bdLocationTable')    
                             
                        @elseif(Session::has('collNearBy'))

                            @include('ghurboMama.partials.locationTables.nearByLocationTable')

                        @elseif(Session::has('collSuggest'))

                            @include('ghurboMama.partials.locationTables.suggestedLocationTable')    

                        @else

                            @include('ghurboMama.partials.locationTables.initVisitLocationTable')

                        @endif
                        </tbody>

                    </table>

                    <hr>
                </div>
            </div>
        </div>
    </div>


        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">

                        @if(Session::has('coll'))

                            {{Session::get('coll')->links()}}

                        @elseif(Session::has('collBd'))

                            {{Session::get('collBd')->links()}}    

                        @elseif(Session::has('collNearBy'))

                            {{Session::get('collNearBy')->links()}}

                        @elseif(Session::has('collSuggest'))

                            {{Session::get('collSuggest')->links()}}

                        @elseif(isset($locations))

                             {{$locations->links()}}   

                        @endif

                    </div>
                </div>
            </div>
        </div>
   
    <hr>
@endsection





@section('script')
        
<script type="text/javascript">
    
    $(document).ready(function()
    {

        var lt = 0;
        var ln = 0;

        var disBydiv =  <?php if(isset($disBydiv)){ echo $disBydiv;  }?> 
        var upBydis  =  <?php if(isset($upBydis)){ echo $upBydis;  }?>

      

        $("#form-divisions").click(function()
        {

            var x = $("#form-divisions option:selected")[0].innerText;

            if(x == "Division")
            {
                $("#form-districts")
                    .empty()
                    .append($("<option>District</option>"));

                $("#form-upazila")
                    .empty()
                    .append($("<option>Upazila</option>"));
            }else
            {
                $("#form-districts").empty();

                $("#form-districts")
                    .empty()
                    .append($("<option>District</option>"));

                $("#form-upazila")
                    .empty()
                    .append($("<option>Upazila</option>"));

                disBydiv.forEach(function(item) {

                    if(item.division == x)
                    {   
                        var el = $("<option>"+item.district+"</option>");
                        $("#form-districts").append(el);
                    }


                });                        
            }

        });


        $("#form-districts").click(function()
        {

            var x = $("#form-districts option:selected")[0].innerText;
            
            if(x == "District")
            {
                $("#form-upazila")
                    .empty()
                    .append($("<option>Upazila</option>"));
            }else
            {
                $("#form-upazila").empty();
                $("#form-upazila")
                    .empty()
                    .append($("<option>Upazila</option>"));

                upBydis.forEach(function(item)
                {

                    if(item.district == x)
                    {   
                        var el = $("<option>"+item.upazila+"</option>");
                        $("#form-upazila").append(el);
                    }

                });                        
            }

        });              

    }
    ); 

</script>

<script type="text/javascript" src="{{ URL::asset('/js/jquery.simpleWeather.js') }}">
</script> 
<script type="text/javascript" src="{{ URL::asset('/js/visitorLocation.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/viewMap.js') }}"></script>   
<script type="text/javascript" src="{{ URL::asset('/js/viewAllNearbyMap.js') }}"></script>   
<script type="text/javascript" src="{{ URL::asset('/js/weatherHome.js') }}"></script>   
  
@endsection	