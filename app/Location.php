<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Division;
use App\District;
use App\Upazila;
use App\Type;
use App\Transport;
use App\Hotel;
use App\Rating;

class Location extends Model
{

    public function division()
    {
    	return $this->belongsTo(Division::class);
    }


	public function district()
    {
    	return $this->belongsTo(District::class);
    }


   	public function upazila()
    {
    	return $this->belongsTo(Upazila::class);
    }

    

    public function transports()
    {
    	return $this->belongsToMany(Transport::class);
    }



    public function types()
    {
    	return $this->belongsToMany(Type::class);
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }
    

}
