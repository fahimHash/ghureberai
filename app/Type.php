<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Location;

class Type extends Model
{
    public function locations()
    {
    	return $this->belongsToMany(Location::class);
    }
}
