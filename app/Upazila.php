<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Location;
use App\District;
use App\Hotel;

class Upazila extends Model
{
    public function district()
    {
    	return $this->belongsTo(District::class);
    }

    public function hotels()
    {
    	return $this->hasMany(Hotel::class);
    }

    public function locations()
    {
    	return $this->hasMany(Location::class);
    }
}
