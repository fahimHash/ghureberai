<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Location;
use App\Upazila;
use App\Division;

class District extends Model
{
    public function locations()
    {
    	return $this->hasMany(Location::class);
    }

    public function upazilas()
    {
    	return $this->hasMany(Upazila::class);
    }

    public function division()
    {
    	return $this->belongsTo(Division::class);
    }        
}
