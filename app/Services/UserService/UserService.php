<?php
namespace App\Services\UserService;
use App\User;

class UserService{

	public $userContainer;



	public function getAllUsers()
	{
		return User::all();
	}



	public function getUserByUserName($uname)
	{
		$this->userContainer = User::where('username','=',$uname)->first();
		return $this;
	}

	public function getUserById($id)
	{
		$this->userContainer = User::where('id','=',$id)->first();
		return $this;
	}



	public function hasDefaultBudget($user)
	{
		if($user->budget != 0){
			return true;
		}

		return false; 
	}

	public function isInHomeDistrict($user_dis,$dis)
	{
		if($user_dis == $dis)
		{
			return true;
		}
		return false;
	}


}