<?php

namespace App\Services\AreaService;
use App\District;
use App\Division;
use App\Upazila;

Class AreaService{


	public function findAllDivisions()
    {
    	$list = Division::all();
    	return $list;
    }

    public function findAllDistricts()
    {
    	$list = District::all();
    	return $list;
    }


    public function findAllUpazilas()
    {
    	$list = Upazila::all();
    	return $list;
    }



//#############


    public function findAllDivisionDistrictPair()
    {
    	$list = self::findAllDivisions();
    	$result = [];
    	$result2 = [];

    	foreach ($list as $k) {
    		$divId = $k->id;
    		$result [$k->division_name] = District::where('division_id','=',$divId)->get();

    		foreach ($result[$k->division_name] as $v) {
    			$result2 []  = array('division'=>$k->division_name,
    								'district'=> $v->district_name );

    		}
    		 
    	}

    	return json_encode($result2);
    }

    public function findAllDistrictUpazilaPair()
    {
    	$list = self::findAllDistricts();
    	$result = [];
    	$result2 = [];

    	foreach ($list as $k) {
    		$disId = $k->id;
    		$result [$k->district_name] = Upazila::where('district_id','=',$disId)->get();

    		foreach ($result[$k->district_name] as $v) {
    			$result2 []  = array('district'=>$k->district_name,
    								'upazila'=> $v->upazila_name );

    		}
    		 
    	}

    	return json_encode($result2);
    }   


//################



    public function districtsByDivision($div)
    {

        global $d; 
        $d= $div;

        $result = District::whereHas('division', function($q) {
            global $d;
            $q->where('division_name','=', $d);
        })->get();

        return $result;
    }



    public function upazilasByDistrict($dis)
    {
        $list = District::all();

        $result = [];

        foreach ($list as $k) {
            if ($k->district->district_name == $dis) {
                $result[]=$k;
            }
        }

        return $result;
    }



//###############



    public function divisionByDistrict($dis)
    {
         
        return District::where('district_name','=',$dis)
                ->first()
                ->division
                ->division_name;
               
    }

    public function districtByUpazila($up)
    {
         
        return Upazila::where('upazila_name','=',$up)
                ->first()
                ->district
                ->district_name;
               
    }

//####################

    public function getDivisionByName($div)
    {
        return Division::where('division_name','=',$div)->first();
    }

    public function getDistrictByName($dis)
    {
        return District::where('district_name','=',$dis)->first();
    }

    public function getUpazilaByName($upz)
    {
        return Upazila::where('upazila_name','=',$upz)->first();
    }    

}