<?php

namespace App\Services\RatingService;
use App\User;
use App\Location;
use App\Rating;

class RatingService{

	public $ratingContainer;

	public function userRatedLocationBefore($u_id,$l_id)
	{
		if(Rating::Where('user_id', '=', $u_id)->Where('location_id', '=', $l_id)->first())
		{
			return true;
		}

		return false;
	}


	public function createNewRating($u_id,$l_id,$ur)
	{
		$rating 			 = new Rating();
		$rating->user_id     = $u_id;
		$rating->location_id = $l_id;
		$rating->rate 		 = $ur;

		$rating->save();

		return $rating;
	}

	public function updateRating($u_id,$l_id,$ur)
	{
		$rating = Rating::Where('user_id', '=', $u_id)->
						  Where('location_id', '=', $l_id)->
						  update(array('rate' => $ur));

		return Rating::Where('user_id', '=', $u_id)->
					   Where('location_id', '=', $l_id)->first();

	}

	public function calculateRating($rt)
	{
		$ratings = Rating::where('location_id', '=', $rt->location_id)->get();
		$count = $ratings->count();
		$sum = $ratings->sum('rate');

		$avg = $sum/$count;
		return (int)$avg;
	}

	public function updateLocationRating($rate,$l_id)
	{
		$loc = Location::find($l_id);
		$loc->rating = $rate;
		$loc->save();
		return $rate;
	}

	public function getRatingsByUserId($id,$rate=null)
	{
		if($rate == null){
			$this->ratingContainer = Rating::where('user_id','=',$id)->get();
		}else{
			$this->ratingContainer = Rating::where('user_id','=',$id)->where('rate','>',$rate)->get();
		}

		return $this; 
	}


}