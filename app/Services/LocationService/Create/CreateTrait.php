<?php
namespace App\Services\LocationService\Create;
use App\Location;

trait CreateTrait{
	/**
	 * Create a location 
	 * params array -> 'en-name','bn-name','cost-trans','cost-accom'
	 * lat, lon, (div,dis,upz)division/district/upazila object, info
	 */

	public function create($array)
	{

		$new_loc 						= new Location();
		$new_loc->location_name 		= $array['en-name'];
		$new_loc->location_name_bangla  = $array['bn-name'];
		$new_loc->cost_transport 		= $array['cost-trans'];
		$new_loc->cost_accommodation 	= $array['cost-accom'];
		$new_loc->latitude 				= $array['lat'];
		$new_loc->longitude 			= $array['lon'];
		$new_loc->location_info 		= $array['info'];

		$new_loc->division()->associate($array['div']);
		$new_loc->district()->associate($array['dis']);
		$new_loc->upazila()->associate($array['upz']);

		$new_loc->location_pic = $array['img'];
		if($array['gallery-path'] != null) { $new_loc->gallery_folder = $array['gallery-path']; }

		return $new_loc->save();

		

	}
}