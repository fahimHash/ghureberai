<?php
namespace App\Services\LocationService\Update;
use App\Location;

trait UpdateTrait{
	/**
	 * update a location by id given in the array
	 * params array -> 'en-name','bn-name','cost-trans','cost-accom'
	 * lat, lon, (div,dis,upz)division/district/upazila object, info
	 */

	public function update($array)
	{

		$loc = Location::where('id','=',$array['id'])->first();

		if($loc->location_name != $array['en-name']) 
		{$loc->location_name = $array['en-name'];}

		if($loc->location_name_bangla  != $array['bn-name']) 
		{$loc->location_name_bangla  = $array['bn-name'];}

		if($loc->cost_transport  != $array['cost-trans']) 
		{$loc->cost_transport  = $array['cost-trans'];}

		if($loc->cost_accommodation  != $array['cost-accom']) 
		{$loc->cost_accommodation  = $array['cost-accom'];}
		
		if($loc->latitude  != $array['lat']) 
		{$loc->latitude  = $array['lat'];}

		if($loc->longitude  != $array['lon']) 
		{$loc->longitude   = $array['lon'];}
		
		if($loc->location_info != $array['info']) 
		{$loc->location_info = $array['info'];}
		
		if($loc->gallery_folder != $array['gallery-path'])
		{ $loc->gallery_folder = $array['gallery-path']; }
		
	
		if($array['img'] != null) { $loc->location_pic = $array['img']; }
		return $loc->save();
	 
						
	}
}