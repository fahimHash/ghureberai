<?php
namespace App\Services\LocationService\Read;
use App\Location;

trait ReadByAreasTrait{

	// By Areas ----------------------------

	/**
	 * get all locations
	 */

	public function allLocations()
	{
		$this->locContainer = Location::all();

        return $this;
	}
	
	/**
	 * get all locations that has the given
	 * division
	 */

	public function locationsByDivision($div)
	{
        global $d; 
        $d= $div;

        $result = Location::whereHas('division', function($q) {
            global $d;
            $q->where('division_name','=', $d);
        })->get();

        $this->locContainer = $result;

        return $this;
	}

	/**
	 * get all locations that has the given
	 * district
	 */

	public function locationsByDistrict($dis)
	{
        global $d; 
        $d= $dis;

        $result = Location::whereHas('district', function($q) {
            global $d;
            $q->where('district_name','=', $d);
        })->get();

        $this->locContainer = $result;

        return $this;
        
	}


	/**
	 * get all locations that has the given
	 * upazila
	 */

	public function locationsByUpazila($up)
	{
        global $d; 
        $d= $up;

        $result = Location::whereHas('upazila', function($q) {
            global $d;
            $q->where('upazila_name','=', $d);
        })->get();

        $this->locContainer = $result;

        return $this;
	}

}	