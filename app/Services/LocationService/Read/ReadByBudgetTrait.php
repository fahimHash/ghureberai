<?php
namespace App\Services\LocationService\Read;
use App\Location;

trait ReadByBudgetTrait{

	// Budget ----------------------------------

	/**
	 * get all locations that has the budget
	 * below the given parameter
	 */

	public function getLocationsByBelowBudget($budget)
	{
		$this->locContainer = Location::where('cost_accommodation','<=', $budget)->get();
	
		return $this;
	}



}	