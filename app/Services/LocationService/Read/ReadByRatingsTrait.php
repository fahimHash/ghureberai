<?php
namespace App\Services\LocationService\Read;
use App\Location;

trait ReadByRatingsTrait{

	// RATINGS -------------------------

	/**
	 * Returns a collection of locations
	 * by rating greater than
	 */

	public function getLocationCollectionByRatingGreaterThan($rate)
	{
		$this->locContainer = Location::where('rating','>',$rate)->get();
		return $this;
	}

	/**
	 * Returns a collection of locations
	 * by rating less than
	 */

	public function getLocationCollectionByRatingLessThan($rate)
	{
		$this->locContainer = Location::where('rating','<',$rate)->get();
		return $this;
	}

	/**
	 * Returns a collection of locations
	 * by exact rating
	 */

	public function locationsByRating($rate)
	{
		$this->locContainer = Location::where('rating','=',$rate)->get();
		return $this;
	}	

}	