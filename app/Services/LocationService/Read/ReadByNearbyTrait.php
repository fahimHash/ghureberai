<?php
namespace App\Services\LocationService\Read;
use App\Location;

trait ReadByNearbyTrait{



	/**
	 * Returns a collection of locations
	 * by looking into users/visitors
	 * curent spot data & the radius
	 * specified 
	 */

	public function getLocationCollectionNearby($dis)
	{
		
		$locs = self::locationsByDistrict($dis);

		$distance = (double)100;

		$this->locContainer = $locs;

		return $this;
				

	}

}	