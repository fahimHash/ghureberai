<?php
namespace App\Services\LocationService\Read;
use App\Location;

trait ReadByVisitsAndInfoTrait{



	public function locationByNameAndIncreaseVisited($name)
	{
		$loc = Location::where('location_name','=',$name)->first();
		$loc->visited = $loc->visited + 1;
		$loc->save(); 
		$this->locContainer = $loc;
		return $this;
	}

	public function getLocationsByVisits($visit)
	{
		$locs = Location::where("visited",'>=',$visit)->orderBy('visited','desc')->get();
		
		$this->locContainer = $locs;
		return $this;
	}



	/**
	 * get all locations that has the given
	 * search parameter only in its name
	 */
	public function searchInName($place)
	{
		$result = Location::where('location_name' , 'like', '%'.$place.'%')->get();

    	return $result;

	}


	/**
	 * get all locations that has the given
	 * search parameter in its name or info
	 */


	public function searchInNameOrInfo($place)
	{
		$result = Location::where('location_info' , 'like', '%'.$place.'%')->
    					  orWhere('location_name' , 'like', '%'.$place.'%')->get();
    	$this->locContainer = $result;
    	return $this;				  
	}

}	