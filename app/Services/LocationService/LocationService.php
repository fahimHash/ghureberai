<?php

namespace App\Services\LocationService;
use App\Location;

use Illuminate\Support\Facades\URL;

Class LocationService{


	use Create\CreateTrait;
	use Update\UpdateTrait;
	use Read\ReadByAreasTrait;
	use Read\ReadByRatingsTrait;
	use Read\ReadByBudgetTrait;
	use Read\ReadByVisitsAndInfoTrait;
	use Read\ReadByNearbyTrait;

	public $locContainer;

	

	public function getLocationById($id)
	{
		$locs = Location::where("id",'=',$id)->get();

		foreach ($locs as $k) {
			$k['division'] = $k->division->division_name;
			$k['district'] = $k->district->district_name;
			$k['upazila'] = $k->upazila->upazila_name;
		}

		$this->locContainer = $locs;
		return $this;
	}


	//Calculations involving distance ---------------------------------

	/**
	 * the function that takes a location 
	 * then takes visitors/users position
	 * co-ordinate and calculate the
	 * location's distance
	 */


	public function haversineGreatCircleDistance
    ($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {

		  $latFrom = deg2rad($latitudeFrom);
		  $lonFrom = deg2rad($longitudeFrom);

		  $latTo = deg2rad($latitudeTo);
		  $lonTo = deg2rad($longitudeTo);

		  $latDelta = $latTo - $latFrom;
		  $lonDelta = $lonTo - $lonFrom;

		  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
		    cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
		  return ($angle * $earthRadius)/1000;
	}

	/**
	 * finds distances for each item in a collection
	 * than forgets the ones that are outside the given 
	 * radius
	 */

	public function calculateDistanceWithRadius($lat,$lon,$radius)
	{


		foreach ($this->locContainer as $y => $k) {

			$distance = self::haversineGreatCircleDistance($lat,$lon,$k->latitude,$k->longitude);

			if($distance <= $radius)
			{
				$k['distance'] = round($distance,2);
			}else
			{
				$this->locContainer->forget($y);
			}

		}

		return $this;
	}

	/**
	 * finds distances for each item in a collection
	 * then just send them all
	 */

	public function calculateDistance($lat,$lon)
	{


		foreach ($this->locContainer as $y => $k) {

			$distance = self::haversineGreatCircleDistance($lat,$lon,$k->latitude,$k->longitude);

			$k['distance'] = round($distance,2);

		}

		return $this;
	}


	/**
	 * get the gallery images for this location
	 */

	public function gallery($location)
	{
		$files_in_this_path = scandir(public_path('location_gallery/'.$location->gallery_folder));
		$images = collect();
		foreach ($files_in_this_path as $value) {

			if($value == '.' || $value == '..'){
				continue;
			}

			$images->push(URL::asset('/location_gallery/'.$location->gallery_folder.'/'.$value));

		}

		return $images;

	}

	public function hasGallery($id)
	{
		if(Location::find($id)->gallery_folder == null)
		{
			return false;
		}

		return true;
	}
	
	public function getGalleryFolder($id)
	{
		return Location::find($id)->gallery_folder;
	}

}