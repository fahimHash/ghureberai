<?php

namespace App\Services\HotelService;
use App\Hotel;
use App\Upazila;

Class HotelService{

	public $hotelContainer;


	public function getHotelsByUpazila($up)
	{
		$upz = Upazila::where('upazila_name','=',$up)->first();
		$this->hotelContainer = $upz->hotels;
		return $this;
	}
}