<?php
namespace App\Services\RecommendationService;

use App\User;
use App\Location;
use App\Services\LocationService\LocationService;


class RecommendationService
{
	protected $locationService;
	

	public $container;


	public function __construct(LocationService $l)
	{
		$this->locationService = $l;
	
	}


	/**
	 * sorts the collection by distance
	 */

	public function sortByDistance($collection,$order=null)
	{
		$this->container = $collection;

		$this->container = array_values(array_sort($this->container, function ($value) {
		    return $value['distance'];
		}));

		if($order == 'desc'){
			$this->container = array_reverse($this->container);
		}
		
		return $this;
	}



	/**
	 * sorts the collection by rating
	 * default upper rating given 3
	 */

	public function sortByRating($collection,$rate=3)
	{
		$this->container = $collection;

		foreach ($this->container as $key => $value) {
			if ($value->rating < $rate)
			{
				$this->container->forget($key);
			}
		}

		$this->container = array_values(array_sort($this->container, function ($value) {
		    return $value['rating'];
		}));

		$this->container = array_reverse($this->container);
		
		return $this;
	}







	public function locationPaginationRequest($collection,$r=null,$itemsPerPage=10)
    {

    	$coll = $collection;
        $page = $r->get('page', 1);    
    
        $perPage = $itemsPerPage;
        $offset = ($page * $perPage) - $perPage;    

        return new \Illuminate\Pagination\LengthAwarePaginator(
            array_slice($coll, $offset, $perPage, true),
            count($coll),
            $perPage, 
            $page, 
            ['path' => $r->url(), 'query' => $r->query()]
        );
    }

} 