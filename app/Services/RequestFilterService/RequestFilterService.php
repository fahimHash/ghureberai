<?php
namespace App\Services\RequestFilterService;
use App\Services\LocationService\LocationService;

class RequestFilterService
{

    public $container;
    

	public function filter($collection=null,$request)
	{

		$coll = $collection;
		$req  = $request;

        $req = array_filter($req,function ($value){
            if($value == "Upazila" || $value == "Type" || $value == "Division" || $value == "District")
            {
                return false;
            }else{
                return $value;
            }
        });


    	if(isset($req['form-upz']))
    	{
    		global $upazila;
    		$upazila = $req['form-upz'];
    		
    		$coll = $coll->filter(function($place){

    			global $upazila;

    			if($place->upazila->upazila_name == $upazila)
    			{
    				return true;
    			}

    		});

    		$this->container = $coll;
            return $this;
    	}



    	if(isset($req['form-dis']))
    	{
    		global $district;
    		$district = $req['form-dis'];
    		
    		$coll = $coll->filter(function($place){

    			global $district;

    			if($place->district->district_name == $district)
    			{
    				return true;
    			}

    		});

    		$this->container = $coll;
            return $this;
    	}



		if(isset($req['form-div']))
    	{
    		global $division;
    		$division = $req['form-div'];
    		
    		$coll = $coll->filter(function($place){

    			global $division;

    			if($place->division->division_name == $division)
    			{
    				return true;
    			}

    		});

    		$this->container = $coll;
            return $this;

    	}

    	$this->container = $coll;
        return $this;
    	

    	// if(isset($req['form-type']))
    	// {
    	// 	$type = $req['form-type'];
    	// 	$query =  DB::table('locations')->where('location_type','=',$type)->union($query); 
    	// }    	

    	
	}
}