<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Location;
use App\District;
use App\Upazila;

class Division extends Model
{

	
    public function locations()
    {
    	return $this->hasMany(Location::class);
    }

    public function districts()
    {
    	return $this->hasMany(District::class);
    }

    public function upazilas()
    {
    	return $this->hasManyThrough(Upazila::class,District::class);
    }
    


}
