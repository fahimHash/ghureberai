<?php

namespace App;
use App\User;
use App\Location;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function location()
    {
    	return $this->belongsTo(Location::class);
    }
}
