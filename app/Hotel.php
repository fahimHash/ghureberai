<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Upazila;

class Hotel extends Model
{

	
	public $timestamps = false;

	public function upazila()
    {
    	return $this->belongsTo(Upazila::class);
    }
}
