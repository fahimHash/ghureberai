<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AreaService\AreaService;
use App\Services\LocationService\LocationService;


class AdminLocationController extends Controller
{
    protected $locationService;
   	protected $areaService;


	public function __construct(LocationService $l,AreaService $a)
	{
		$this->locationService = $l;
		$this->areaService = $a;
	}

	public function createLocation(Request $r)
	{
		
		// dd($r->all());		
		
		//validate	
		$this->validate($r,[
			'form-en-name'          => 'required|max:40',
			'form-bn-name'     		=> 'required|max:40',
			'form-div'     			=> 'required|exists:divisions,division_name',
			'form-dis'     			=> 'required|exists:districts,district_name',
			'form-upz'     			=> 'required|exists:upazilas,upazila_name',
			'form-cost-trans'     	=> 'required|numeric',
			'form-cost-accom'     	=> 'required|numeric',
			'form-lat'      		=> 'required|numeric',
			'form-lon'      		=> 'required|numeric',
			'form-description'      => 'required',
			'form-loc-img'			=> 'required|image',
		]);



		//validating gallery images
		if(isset($r['form-loc-img-gallery']))
		{
			foreach ($r->file('form-loc-img-gallery') as $k) {
				if (!(substr($k->getMimeType(), 0, 5)=='image')) {
					return redirect()
						           ->back()
						           ->with('info','One of the files you choose for gallery isn\'t an image file');

				}

			}
		}



		//creating the gallery

		$path = strtolower($r['form-en-name']).'-'.str_random(5);

		foreach ($r->file('form-loc-img-gallery') as $k) {
			$k->move('location_gallery/'.$path ,$k->getClientOriginalName());
		}




		//checking main image and putting it in public/location_pictures
		if($r->file('form-loc-img') != null && 
		   substr($r->file('form-loc-img')->getMimeType(), 0, 5)=='image')
		{
			$img = $r->file('form-loc-img')->getClientOriginalName();
			$r->file('form-loc-img')->move('location_pictures',$img);
			
		}else{
			$img = null;	
		}


		//get area models according to location input areas
		$division = $this->areaService->getDivisionByName($r['form-div']);
		$district = $this->areaService->getDistrictByName($r['form-dis']);
		$upazila  = $this->areaService->getUpazilaByName($r['form-upz']);


	

		// make the array to pass for location creation
		$array = [
			'en-name' 	 	=> $r['form-en-name'],
			'bn-name' 	 	=> $r['form-bn-name'],
			'div' 		 	=> $division,
			'dis' 		 	=> $district,
			'upz' 		 	=> $upazila,
			'cost-trans' 	=> $r['form-cost-trans'],
			'cost-accom' 	=> $r['form-cost-accom'],
			'lat' 		 	=> $r['form-lat'],
			'lon' 		 	=> $r['form-lon'],
			'info' 		 	=> $r['form-description'],
			'img'		 	=> $img,
			'gallery-path'  => $path,
		];

		// create
		if($this->locationService->create($array)){
			return redirect()->back()
						     ->with('info','The Location has been added successfully');
		}

			return redirect()->back();


	}




	public function editLocation(Request $r)
	{
		

		//validate	
		$this->validate($r,[
			'id' => 'required|numeric|exists:locations,id'
		]);


		//validating gallery images
		if(isset($r['form-loc-img-gallery']))
		{
			foreach ($r->file('form-loc-img-gallery') as $k) {
				if (!(substr($k->getMimeType(), 0, 5)=='image')) {
					return redirect()
						           ->back()
						           ->with('info','One of the files you choose for gallery isn\'t an image file');

				}

			}
		}


		//creating the gallery
		if(!$this->locationService->hasGallery($r['id']))
		{
			$path = strtolower($r['form-en-name']).'-'.str_random(5);
		}else{
			$path = $this->locationService->getGalleryFolder($r['id']);
		}
		

		foreach ($r->file('form-loc-img-gallery') as $k) {
			$k->move('location_gallery/'.$path ,$k->getClientOriginalName());
		}



		//checking featured image and putting in disk
		if($r->file('form-loc-img') != null && 
		   substr($r->file('form-loc-img')->getMimeType(), 0, 5)=='image')
		{
			$img = $r->file('form-loc-img')->getClientOriginalName();
			$r->file('form-loc-img')->move('location_pictures',$img);
			
		}else{
			$img = null;	
		}




		// make the array to pass for location creation
		$array = [
			'en-name' 	 	=> $r['form-en-name'],
			'bn-name' 	 	=> $r['form-bn-name'],
			'cost-trans' 	=> $r['form-cost-trans'],
			'cost-accom'    => $r['form-cost-accom'],
			'lat' 		 	=> $r['form-lat'],
			'lon' 		 	=> $r['form-lon'],
			'info' 		 	=> $r['form-description'],
			'img'		 	=> $img,
			'id'		 	=> $r['id'],
			'gallery-path'  => $path
		];

		if($this->locationService->update($array)){
			return redirect()->back()->with('info','Location Updated successfully');
		};

	}



	public function getLocationDataForEdit(Request $r)
	{
		$id = $r['id'];
		return $this->locationService->getLocationById($id)->locContainer;		
	}
}
