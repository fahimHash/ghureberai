<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function dashboard(Request $r)
    {
    	$visitorCount = DB::table('location_tracker_data')
    							->groupBy('ip')
    							->pluck('ip')
    							->count();
    	$userCount = DB::table('users')
    							->get()
    							->count();
    							
    	return view('ghurboMama.admin.home',compact('visitorCount','userCount'));
    }
}
