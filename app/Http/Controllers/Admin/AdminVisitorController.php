<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AdminVisitorController extends Controller
{


    
    public function showHeatMap(Request $r)
    {
    	$mapData = DB::table('location_tracker_data')->get();
    	
    	return view('ghurboMama.admin.visitor.heatMap',['mapData' => $mapData]);
    }


    public function showDivisionChart(Request $r)
    {
        $divisions = DB::table('location_tracker_data')
        			->select('division', DB::raw('count(*) as total'))
        			->groupBy('division')
        			->get();


       
         return view('ghurboMama.admin.visitor.chart-division', compact('divisions'));
    	
    }

    public function showAreaChartByDivision(Request $r)
    {
        $collection = DB::table('location_tracker_data')
                    ->where('division','=',$r['visitor-div-area'])
                    ->get()->groupBy('district');

        $areas = collect();
        $division = $r['visitor-div-area'];
        foreach ($collection as $key => $value) {
              $areas->push(array($key,$value->count()));          
        }      

        

        return view('ghurboMama.admin.visitor.chart-division-areas',compact('areas','division'));
    }
}
