<?php

namespace App\Http\Controllers\Admin;

use App\Services\UserService\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminUserController extends Controller
{

	protected $us;

	public function __construct(UserService $u)
	{
		$this->us = $u;
	}

    public function users(Request $r)
    {
    	$users = $this->us->getAllUsers();
    	return view('ghurboMama.admin.user.users',compact('users'));
    }
}
