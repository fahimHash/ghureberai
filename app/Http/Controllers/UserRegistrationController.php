<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserRegistrationController extends Controller
{
	public function getRegister()
	{
		return view('ghurboMama.auth.user-register');
	}

	public function postRegister(Request $r)
	{
		//validation
		$this->validate($r,[
			'email'          => 'required|unique:users|email|max:255',
			'first_name'     => 'required|alpha_dash|max:255',
			'last_name'      => 'required|alpha_dash|max:255',
			'password'       => 'required|min:6',
		]);

		//registration
		$user = new User();
		$user->first_name    = $r['first_name'];
		$user->last_name     = $r['last_name'];
		$user->username      = $r['first_name'].rand(0,10000).$r['last_name'];
		$user->email         = $r['email'];
		$user->password      = bcrypt($r['password']);

		if($r['default_budget']==null)
		{
			$user->budget = 0;	
		}else{
			$user->budget = htmlspecialchars($r['default_budget']);
		}
		
		

		$user->save();

		return redirect()->route('login')->with('info','Your account has been created, now you can login');
	}
}
