<?php
namespace App\Http\Controllers;
use Auth;
use App\User;
use Illuminate\Http\Request;

class UserLoginController extends Controller
{
 	public function getLogin(Request $r)
	{
		return view('ghurboMama.auth.user-login');
	}

	public function postLogin(Request $r)
	{
		//validation
		$this->validate($r,[
			'email' => 'required|email|max:255',
			'password' => 'required|min:6',
		]);
		//login


		if(!Auth::attempt($r->only(['email','password'])))
		{
			return redirect()->back()->with('info','Could not log you in with those credentials');
		}

		return redirect()->route('home')->with('info','Welcome');
	}

	public function logout(){
		Auth::logout();
		return redirect()->route('home');
	}
}
