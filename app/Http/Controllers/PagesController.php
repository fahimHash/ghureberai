<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Services\AreaService\AreaService;
use App\Services\HotelService\HotelService;
use App\Services\LocationService\LocationService;
use App\Services\RecommendationService\RecommendationService;

class PagesController extends Controller
{

	protected $locationService;
	protected $areaService;
	protected $hotelService;
	protected $recomService;



	public function __construct(LocationService $l,AreaService $a,HotelService $h,RecommendationService $rc)
	{
		$this->locationService = $l;
		$this->areaService = $a;
		$this->hotelService = $h;
		$this->recomService = $rc;
	}




    public function home(Request $r)
    {
    	$area = new AreaService();

		  $disBydiv = $area->findAllDivisionDistrictPair();
    	$upBydis  =$area->findAllDistrictUpazilaPair();

    	$locations = $this->locationService->getLocationsByVisits(5)->locContainer;

    	$locations = array_values(array_sort($locations, function ($value) {
  		    return $value['visited'];
  		}));

    	$locations = array_reverse($locations);
    	$locations = $this->recomService->locationPaginationRequest($locations,$r);
    	
    	return view('ghurboMama.home',['disBydiv' => $disBydiv,
    								                 'upBydis'=> $upBydis,
    								                 'locations' => $locations]
                                    );
    }

  



	public function location(Request $r)
    {
  		$requested_location_name = Route::current()->parameters['value'];

  		$requested_location = $this->locationService
  							->locationByNameAndIncreaseVisited($requested_location_name)
  							->locContainer;

      if (!$requested_location) {
        return redirect()->route('home');
      }

		
  		$up          = $requested_location->upazila->upazila_name;
  		$req_loc_lat = $requested_location->latitude;
  		$req_loc_lon = $requested_location->longitude;
		
  		$nearest_locations_by_upazila = $this->locationService
                    											 ->locationsByUpazila($up)
                    											 ->calculateDistance($req_loc_lat,$req_loc_lon)
                    											 ->locContainer;
		


  		foreach ($nearest_locations_by_upazila as $key => $value) {

  			if($value->location_name == $requested_location_name)
  			{
  				$nearest_locations_by_upazila->forget($key);
  			}

  		}


		
  		$nearest_locations_by_upazila = $nearest_locations_by_upazila
  										->sortBy(function($c){
  											return $c->distance;
  										})->take(5);

		

		  $hotels = $this->hotelService->getHotelsByUpazila($up)->hotelContainer->take(5);

      if(!$requested_location->gallery_folder == null)
      {
        $gallery_images = $this->locationService->gallery($requested_location);  
      }
      else{
        $gallery_images = null;
      }
      

      		

    	return view( 'ghurboMama.location-details',['location' => $requested_location,
    							 'nearest' => $nearest_locations_by_upazila,
    							 'hotels' => $hotels,
                   'images' => $gallery_images]);
      }




    public function getCreateLocationPage()
    {
    	$area = new AreaService();

		  $disBydiv = $area->findAllDivisionDistrictPair();
    	$upBydis  =$area->findAllDistrictUpazilaPair();

    	return view('ghurboMama.locations.create',['disBydiv' => $disBydiv,
    												'upBydis'=> $upBydis]);
    }




    public function getUpdateLocationPage(Request $r)
    {

    	$area = new AreaService();

		  $disBydiv = $area->findAllDivisionDistrictPair();
    	$upBydis  =$area->findAllDistrictUpazilaPair();



    	$locs = $this->locationService->allLocations()->locContainer;
    	$locs = array_values(array_sort($locs, function ($value) {
		    return $value['location_name'];
		}));

    	$locs = $this->recomService->locationPaginationRequest($locs,$r,25); 

    	return view('ghurboMama.locations.update',['locations' => $locs,
    												'disBydiv' => $disBydiv,
    												'upBydis'=> $upBydis]);
    }    
}
