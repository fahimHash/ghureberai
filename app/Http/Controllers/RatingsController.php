<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\RatingService\RatingService;


class RatingsController extends Controller
{

	protected $ratingService;


	public function __construct(RatingService $rt)
	{
		$this->ratingService = $rt;
	}



    public function returnFeedback(Request $r){
    	return [$r->userRatingOnLeave,$r->user,$r->location];
    }



    public function changeRatingOnUsersVote(Request $r){
    	//valdiate

    	//take inputs
    	$user_rating     = $r->userRatingOnLeave;
    	$user_id 	= $r->user;
    	$location_id = $r->location;

    	//do magic
    	if($this->ratingService->userRatedLocationBefore($user_id,$location_id))
    	{
			$rating = $this->ratingService->updateRating($user_id,$location_id,$user_rating);
			 
    	}else{
    		$rating=$this->ratingService->createNewRating($user_id,$location_id,$user_rating);
    	}

    	
    	$rate = $this->ratingService->calculateRating($rating);
    	$rate = $this->ratingService->updateLocationRating($rate,$location_id);

    	return $rate;

    }
}
