<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Services\UserService\UserService;
use App\Services\RatingService\RatingService;

class UserController extends Controller
{

	protected $userService;
	protected $ratingService;

    public function __construct(UserService $u,RatingService $rt)
    {
    	$this->userService = $u;
    	$this->ratingService = $rt;
    }

    public function getProfile(Request $r)
    {
    	
    	$id = $r->user()->id;

    	$user    = $this->userService->getUserById($id)->userContainer;
    	$ratings = $this->ratingService->getRatingsByUserId($id,4)->ratingContainer;
    	
    	
    	return view('ghurboMama.user.profile',['user' => $user,'ratings'=>$ratings]);
    }

}
