<?php

namespace App\Http\Controllers\Visitor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class VisitorController extends Controller
{
    public function insertTrackingData(Request $r)
    {
    	if(! DB::table('location_tracker_data')
    		->insert(array('lat' => $r['lat'], 'lon' => $r['lon'], 'district' => $r['dis'],
                            'division' => $r['div'],
                            'ip' => $r->ip(),
    						'created_at' =>  Carbon::now(),
            				'updated_at' =>  Carbon::now()))
    	  )
    	{
    		return 'failed';
    	}
    	return 'Success';	
    }

    
}
