<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;


class ExcelController extends Controller
{
    public function get()
    {
    	return view('excel/excel_upload');
    }

    public function process(Request $r)
    {
    	// $extension = $r->fileToUpload->getClientOriginalName();
    	// echo $extension;
    	 //dd($r['fileToUpload']);
    	// echo $r->file('fileToUpload')->getClientOriginalName();
    	 
    	 // $name = explode('.', $r->file('fileToUpload')->getClientOriginalName())[0];
    	 // $ext = explode('.', $r->file('fileToUpload')->getClientOriginalName())[1];

    	 // $fileName = $name.'.'.$ext;
 


    	 //echo asset('uploads/'.$r->file('fileToUpload')->getClientOriginalName());

    	//Storage::put($r->file('fileToUpload')->getClientOriginalName(), $r['fileToUpload']);
    	 $destinationPath = 'uploads';

    	 $r['fileToUpload']
    	 ->move($destinationPath, 
    	 		$r->file('fileToUpload')->getClientOriginalName());


		$results = Excel::selectSheets('Location')
				->load(public_path('uploads/'.$r->file('fileToUpload')->getClientOriginalName()));

		$rowColl = $results->get()[0];

		foreach ($rowColl as $key => $value) {

			if(
				($value['location_name']==null) &&
				($value['division_id']==null) 	&&
				($value['district_id']==null) 	&&
				($value['upazila_id']==null) 
			)
			{
				$rowColl->forget($key);
			}
		}

		foreach ($rowColl as $k) {
			
				DB::table('excel')->insert(
				    ['location_name' => $k->location_name, 
				     'location_name_bangla' => $k->location_name_bangla,
				     'division_id' => $k->division_id,
				     'district_id' => $k->district_id,
				     'upazila_id' => $k->upazila_id,
				     'location_pic' => $k->location_pic,
				     'cost_transport' => $k->cost_transport,
				     'cost_accommodation' => $k->cost_accommodation,
				     'location_info' => $k->location_info
				    ]
				);
			
			
		}
		

    }

    
}
