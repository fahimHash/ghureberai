<?php

namespace App\Http\Controllers\Location;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Services\LocationService\LocationService;
use App\Services\RecommendationService\RecommendationService;
use App\Services\RequestFilterService\RequestFilterService;

class LocationSearchController extends Controller
{

	protected $locationService;
    protected $recomService;
    protected $requestFilterService;



	public function __construct(LocationService $l,RecommendationService $rc,RequestFilterService $cf)
	{
		$this->locationService             = $l;
        $this->recomService                = $rc;
        $this->requestFilterService        = $cf;

	}

    public function searchByDivision(Request $r)
    {
        $division = Route::current()->parameters['division'];
        $locations = $this->locationService->locationsByDivision($division)->locContainer;


        $locations = array_values(array_sort($locations, function ($value) {
            return $value['location_name'];
        }));

        $locations = $this->recomService->locationPaginationRequest($locations,$r);
        
        return view('ghurboMama.division.locationListDivision',[
                                       'locations' => $locations,
                                       'division' => $division]);
    }



    public function searchByDistrict(Request $r)
    {
        $district = Route::current()->parameters['district'];
        $locations = $this->locationService->locationsByDistrict($district)->locContainer;


        $locations = array_values(array_sort($locations, function ($value) {
            return $value['location_name'];
        }));

        $locations = $this->recomService->locationPaginationRequest($locations,$r);
        
        return view('ghurboMama.district.locationListDistrict',[
                                       'locations' => $locations,
                                       'district' => $district]);
    }

    public function searchByUpazila(Request $r)
    {

        $upazila = Route::current()->parameters['upazila'];
        $locations = $this->locationService->locationsByUpazila($upazila)->locContainer;


        $locations = array_values(array_sort($locations, function ($value) {
            return $value['location_name'];
        }));

        $locations = $this->recomService->locationPaginationRequest($locations,$r);
        
        return view('ghurboMama.upazila.locationListUpazila',[
                                       'locations' => $locations,
                                       'upazila' => $upazila]);
    }

    public function locationsUserSearchedForInBd(Request $r)
    {

    	$reqData   = $r->all();
    	$place     = $reqData['form-searchBd'];
		$curr_lat  = (float)$r['lat'];
		$curr_lon  = (float)$r['lon'];
        

        
    	$service = $this->locationService->searchInNameOrInfo($place)
                                         ->calculateDistance($curr_lat,$curr_lon);

        $service = $this->recomService->sortByRating($service->locContainer,0);

        $coll    = $service->container;
        $coll = $this->recomService->locationPaginationRequest($coll,$r);
   		
        
    	return redirect()->route('home')->with(['collBd'=>$coll]);    	

    }



    public function locationsSearched(Request $r)
    {

        $reqData   = $r->all();
        $budget = (int)$reqData['form-budget'];
        $curr_lat  = (float)$r['lat'];
        $curr_lon  = (float)$r['lon'];


        
        // is a user, give him results of his search
        if(($r->user() != null)  && ($budget > 500))
        {
            $coll = $this->performSearch($budget,$curr_lat,$curr_lon,$reqData,true);
            $coll = $this->recomService->locationPaginationRequest($coll,$r);

            return redirect()->route('home')->with(['coll'=>$coll]); 
        }

        elseif(($r->user() != null)  && ($budget <= 500))
        {
            if($r->user()->budget > 0){

                $budget = $r->user()->budget;       

            }

            $coll = $this->performSearch($budget,$curr_lat,$curr_lon,$reqData,true);
            $coll = $this->recomService->locationPaginationRequest($coll,$r);

            return redirect()->route('home')->with(['coll'=>$coll]);

        }


        // not a user, give him results of his search
        $coll = $this->performSearch($budget,$curr_lat,$curr_lon,$reqData);
        $coll = $this->recomService->locationPaginationRequest($coll,$r);


        return redirect()->route('home')->with(['coll'=>$coll]);             
        

    }





    public function performSearch($budget,$curr_lat,$curr_lon,$reqData,$user=false)
    {
        $coll = $this->locationService
                     ->getLocationsByBelowBudget($budget)
                     ->calculateDistance($curr_lat,$curr_lon);


        $service = $this->requestFilterService->filter($coll->locContainer,$reqData);


        if($user)
        {
            $service = $this->recomService->sortByRating($service->container);
            
        }else{
            $service = $this->recomService->sortByDistance($service->container);
            
        }
        
        $coll = $service->container;
        return $coll;
    }

    




    public function NearbyOrSuggestMe(Request $r)
    {
        if($r->user() != null)
        {

            if(isset($r['form-submit-suggest']))
            {
                return $this->suggestToUser($r);
            }
            
        }

        return $this->locationsNearby($r);

    }



    

    public function locationsNearby(Request $r)
    {

        $reqData   = $r->all();
        $dis = $r['dis'];
        $lat = $r['lat'];
        $lon = $r['lon'];

        $radius = (double)20.0;

        $coll = $this->locationService
                     ->locationsByDistrict($dis)
                     ->calculateDistanceWithRadius($lat,$lon,$radius);

        $service = $this->recomService->sortByDistance($coll->locContainer);

        $coll = $service->container;

        $coll = $this->recomService->locationPaginationRequest($coll,$r);

        return redirect()->route('home')->with(['collNearBy'=>$coll]);
              
    }




    public function suggestToUser(Request $r)
    {

        $reqData   = $r->all();
        $lat = $r['lat'];
        $lon = $r['lon'];
    

        if($r->user()->budget > 0)
        {
            $coll = $this->locationService
                         ->getLocationsByBelowBudget($r->user()->budget)
                         ->calculateDistance($lat,$lon);


            $service = $this->recomService->sortByRating($coll->locContainer);
            $coll = $service->container;
            $coll = $this->recomService->locationPaginationRequest($coll,$r);

            return redirect()->route('home')->with(['collSuggest'=>$coll]);
            
        }

        $coll = $this->locationService
                     ->getLocationCollectionByRatingGreaterThan(3)
                     ->calculateDistance($lat,$lon);

        $service = $this->recomService->sortByDistance($coll->locContainer);
        $coll = $service->container;
        $coll = $this->recomService->locationPaginationRequest($coll,$r);

        return redirect()->route('home')->with(['collSuggest'=>$coll]);

    }


}

