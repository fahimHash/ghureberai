<?php

namespace App\Http\Controllers\Location;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Services\LocationService\LocationService;
use App\Services\AreaService\AreaService;

class LocationController extends Controller
{
   	protected $locationService;
   	protected $areaService;


	public function __construct(LocationService $l,AreaService $a)
	{
		$this->locationService = $l;
		$this->areaService = $a;
	}



	public function createLocation(Request $r)
	{
		

		//validate	
		$this->validate($r,[
			'form-en-name'          => 'required|max:40',
			'form-bn-name'     		=> 'required|max:40',
			'form-div'     			=> 'required|exists:divisions,division_name',
			'form-dis'     			=> 'required|exists:districts,district_name',
			'form-upz'     			=> 'required|exists:upazilas,upazila_name',
			'form-cost-trans'     	=> 'required|numeric',
			'form-cost-accom'     	=> 'required|numeric',
			'form-lat'      		=> 'required|numeric',
			'form-lon'      		=> 'required|numeric',
			'form-description'      => 'required|alpha_dash',
			'form-loc-img'			=> 'required|image'
		]);

		
		//get models according to inputs
		$division = $this->areaService->getDivisionByName($r['form-div']);
		$district = $this->areaService->getDistrictByName($r['form-dis']);
		$upazila  = $this->areaService->getUpazilaByName($r['form-upz']);



		//checking image and putting in disk
		if($r->file('form-loc-img') != null && 
		   substr($r->file('form-loc-img')->getMimeType(), 0, 5)=='image')
		{
			$img = $r->file('form-loc-img')->getClientOriginalName();
			$r->file('form-loc-img')->move('location_pictures',$img);
			
		}else{
			$img = null;	
		}

		
		

		// make the array to pass for location creation
		$array = [
			'en-name' 	 => $r['form-en-name'],
			'bn-name' 	 => $r['form-bn-name'],
			'div' 		 => $division,
			'dis' 		 => $district,
			'upz' 		 => $upazila,
			'cost-trans' => $r['form-cost-trans'],
			'cost-accom' => $r['form-cost-accom'],
			'lat' 		 => $r['form-lat'],
			'lon' 		 => $r['form-lon'],
			'info' 		 => $r['form-description'],
			'img'		 => $img
		];


		// create
		if($this->locationService->create($array)){
			return redirect()->back()
						     ->with('info','The Location has been added successfully');
		}

			return redirect()->back();


	}




	public function editLocation(Request $r)
	{
		

		//validate	
		$this->validate($r,[
			'id' => 'required|numeric|exists:locations,id'
		]);


		//checking image and putting in disk
		if($r->file('form-loc-img') != null && 
		   substr($r->file('form-loc-img')->getMimeType(), 0, 5)=='image')
		{
			$img = $r->file('form-loc-img')->getClientOriginalName();
			$r->file('form-loc-img')->move('location_pictures',$img);
			
		}else{
			$img = null;	
		}

		// make the array to pass for location creation
		$array = [
			'en-name' 	 => $r['form-en-name'],
			'bn-name' 	 => $r['form-bn-name'],
			'cost-trans' => $r['form-cost-trans'],
			'cost-accom' => $r['form-cost-accom'],
			'lat' 		 => $r['form-lat'],
			'lon' 		 => $r['form-lon'],
			'info' 		 => $r['form-description'],
			'img'		 => $img,
			'id'		 => $r['id']
		];

		if($this->locationService->update($array)){
			return redirect()->back()->with('info','Location Updated successfully');
		};

	}



	public function getLocationDataForEdit(Request $r)
	{
		$id = $r['id'];
		return $this->locationService->getLocationById($id)->locContainer;		
	}
}
