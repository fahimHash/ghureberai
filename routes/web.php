<?php

use App\Division;
use App\Type;
use App\Transport;
use App\Location;
use App\District;
use App\Upazila;
use App\Hotel;
use Illuminate\Support\Facades\DB;

use App\Services\AreaService\AreaService;
use App\Services\LocationService\LocationService;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;



//Pages
Route::get('/', 'PagesController@home')->name('home');
Route::get('/bang', 'PagesController@home2')->name('home2');
Route::get('/location/{value}', 'PagesController@location');



//Login/Registration
Route::get('/register', 'UserRegistrationController@getRegister')->name('register');
Route::post('/register', 'UserRegistrationController@postRegister');


Route::get('/login', 'UserLoginController@getLogin')->name('login');
Route::post('/login', 'UserLoginController@postLogin');

Route::get('/logout', 'UserLoginController@logout')->name('logout');






//User------------------------------------------------------------------

Route::get('/user/profile','UserController@getProfile')->middleware('auth')
	 ->name('UserProfile');







//Visitor---------------------------------------------------------------

//Visitor Location Track
Route::get('/trackVisitor','Visitor\VisitorController@insertTrackingData');	 






//Searches--------------------------------------------------------------

//location
Route::get('/searchbd','Location\LocationSearchController@locationsUserSearchedForInBd')
		->name('locationsUserSearchedForInBd');

Route::get('/search','Location\LocationSearchController@locationsSearched')
		->name('locationsUserSearchedFor');

Route::get('/searchNear','Location\LocationSearchController@NearbyOrSuggestMe')
		->name('locationsNearby');


// Division, District & Upazila

Route::get('/division/locations/{division}','Location\LocationSearchController@searchByDivision')
	 ->name('searchByDivision');

Route::get('/district/locations/{district}','Location\LocationSearchController@searchByDistrict')
	 ->name('searchByDistrict');

Route::get('/upazila/locations/{upazila}','Location\LocationSearchController@searchByUpazila')
	 ->name('searchByUpazila');	







// Rating------------------------------------------------------------

Route::get('/submitRating','RatingsController@changeRatingOnUsersVote')->middleware('auth')	->name('submitRating');







//Admin--------------------------------------------------------------

Route::get('/admin','Admin\AdminController@dashboard');

//Admin-Visitor
Route::get('/admin/visitor/heatmap','Admin\AdminVisitorController@showHeatMap')
	 ->name('admin.visitor.heatmap');

Route::get('/admin/visitor/chart/division','Admin\AdminVisitorController@showDivisionChart')->name('admin.visitor.chartDivision');

Route::get('/admin/visitor/chart/division/areas',
			'Admin\AdminVisitorController@showAreaChartByDivision')
	   ->name('admin.visitor.division.areas');	 


//Admin-User
Route::get('/admin/users','Admin\AdminUserController@users')
	 ->name('admin.user.users');	   

//Admin-Location
Route::get('/admin/locations/add','PagesController@getCreateLocationPage')
	 ->name('addLocation');

Route::post('/admin/locations/add','Admin\AdminLocationController@createLocation');



Route::get('/admin/locations/update','PagesController@getUpdateLocationPage')
	 ->name('updateLocation');

Route::post('/admin/locations/update','Admin\AdminLocationController@editLocation')
	 ->name('editLocation');

Route::get('/admin/locations/update/id','Admin\AdminLocationController@getLocationDataForEdit')
	 ->name('getLocationDataForEdit');










//Excel-------------------------------------------------------

Route::get('/ex','ExcelController@get')->name('excel.form');
// Route::get('/ex2','ExcelController@ajax')->name('excel.form.ajax');
Route::post('/ex','ExcelController@process');



//TEST---------------------------------------------------

Route::get('/test',function(){
	//dump(public_path());
	// echo Request::getHost();

	//echo URL::asset('/location_picture/');
	// dd(new \App\Http\Controllers\Location\LocationController());

	//dd(app('session.store'));
	//dd(app('auth'));
	 //dd(public_path());
	



	// $data = scandir(public_path('location_gallery/washington-CZsW7'));
	// $folder = 'washington-CZsW7';
	// foreach ($data as $value) {
	// 	if($value == '.' || $value == '..'){
	// 		continue;
	// 	}

	// 	dump(URL::asset('/location_gallery/'.$folder.'/'.$value));
		
	// }
	// die();

	dd(\App\Location::find(1397));
	
	$bounds = array('raj' => ['lower_lat' => '23.7985388','upper_lat' => '25.2159939',
							  'lower_lon' => '88.0113889','upper_lon' => '89.7901011'],
					'khu' => ['lower_lat' => '21.6379626','upper_lat' => '24.2097079',
							  'lower_lon' => '88.570617','upper_lon' => '89.988395'],
					'bar' => ['lower_lat' => '21.7301615','upper_lat' => '23.0715591',
							  'lower_lon' => '89.8594281','upper_lon' => '91.0169218'],
					'dha' => ['lower_lat' => '22.8457651','upper_lat' => '24.7891189',
							  'lower_lon' => '89.3020689','upper_lon' => '91.2582148'],
					'syl' => ['lower_lat' => '23.97757','upper_lat' => '25.2041646',
							  'lower_lon' => '90.9354257','upper_lon' => '92.494232'],
					'mym' => ['lower_lat' => '24.2473579','upper_lat' => '25.434721',
							  'lower_lon' => '89.643391','upper_lon' => '91.24467'],
					'chi' => ['lower_lat' => '20.5756375','upper_lat' => '24.27206',
							  'lower_lon' => '90.5353387','upper_lon' => '92.67366'],
					'rang' => ['lower_lat' => '25.037387','upper_lat' => '26.631954',
							  'lower_lon' => '88.0861999','upper_lon' => '89.8903941']		  		  		  		  		  		  		  );

	// $faker = \Faker\Factory::create();

	// $divs = Division::select('division_name')->get();
	// $divisions=[];

	// $dis = District::select('district_name')->get();
	// $districts = [];

	// foreach ($dis as $k) {
	// 	$districts[] = $k->district_name;
	// }

	// foreach ($divs as $k) {
	// 	$divisions[] = $k->division_name;
	// }
	

	// foreach (range(1,50) as $k) {

		
	// 	\Illuminate\Support\Facades\DB::table('location_tracker_data')
 //    		->insert(array('lat' => $faker->latitude('20.6173999','26.633914'), 
 //    					   'lon' => $faker->longitude('88.0086141','92.6801153'),
 //    					   'ip' => $faker->ipv4,
 //    					   'division' => $faker->randomElement($divisions),
 //    					   'district' => $faker->randomElement(array('Kustia','Netrokona','Bogra')),
 //    					   'created_at' =>  \Carbon\Carbon::now(),
 //            			   'updated_at' =>  \Carbon\Carbon::now()));
	// }

	// return config('app.url');

	
});