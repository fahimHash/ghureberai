

$('.vmap').click(function(){

    var targetLat=parseFloat($('#lat').attr('data-lat'));
    var targetLon=parseFloat($('#lon').attr('data-lon'));
    


    element = document.getElementById('gmap');

    navigator.geolocation.getCurrentPosition(function(position)
    {
    	

    	var target = {lat: targetLat, lng: targetLon};
    	var visitor = {lat: position.coords.latitude, 
    				   lng: position.coords.longitude};

    	google.maps.visualRefresh = true;	

    	//creating map object	

    	var options = {
    		center: visitor, 
    		zoom : 10,
    		mapTypeId : google.maps.MapTypeId.ROADMAP
    	};

      
    	var map = new google.maps.Map(element,options);
    	var bounds = new google.maps.LatLngBounds();
    	var infowindow = new google.maps.InfoWindow({ content : "<b>You</b>" });



    	

    	// setting marker for visitor
    	options = {position : visitor, map : map};
    	var visitor_marker = new google.maps.Marker(options);
    	infowindow.open(map,visitor_marker);
    	bounds.extend(visitor);


    	//setting marker for target
    	options = {position : target, map : map};
    	var target_marker = new google.maps.Marker(options);
    	bounds.extend(target);

    	
    	map.fitBounds(bounds);


    	var directionsService = new google.maps.DirectionsService();
    	var directionsRequest = {
		  origin: visitor,
		  destination: target,
		  travelMode: google.maps.DirectionsTravelMode.DRIVING,
		  unitSystem: google.maps.UnitSystem.METRIC
		};

    	directionsService.route(
		  directionsRequest,
		  function(response, status)
		  {
		    if (status == google.maps.DirectionsStatus.OK)
		    {
		      new google.maps.DirectionsRenderer({
		        map: map,
		        directions: response,
		        polylineOptions: {
			      strokeColor: "red"
			    }
		      });
		    }
		    else{
		    	console.log("Coumd not find the route");
		    }
		  }
		);
    	

    }
    );
}
);