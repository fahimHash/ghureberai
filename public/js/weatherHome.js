function loadWeather(location, woeid) {
  $.simpleWeather({
    location: location,
    woeid: woeid,
    unit: 'f',
    success: function(weather) {
      html = '<h3><i class="icon-'+weather.code+' fa fa-cloud fa-2x"></i><br><span style="color:#4cae4c">'+weather.alt.temp+'&deg;C';
      html += '</span><span> in '+weather.region+' today.</span></h3>';
      html += '<h5 class="currently">And it is more likely to be '+weather.currently+' in your area now or later sometime.</h5>';
       
      
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('<p>'+error+'</p>');
    }
  });
}

$(document).ready(function() {

  navigator.geolocation.getCurrentPosition(function(position)
    {
        loadWeather(position.coords.latitude+','+position.coords.longitude);
    });
});

