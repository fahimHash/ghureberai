$('#showAllMap').click(function(){

	
	lats = $('.loclat');
	lons = $('.loclon');


	var locations = [];

	for (var i = 0; i<lats.length; i++){
		locations.push({lat : parseFloat(lats[i].dataset.loclat), 
						lng : parseFloat(lons[i].dataset.loclon)
					   });
	}

  

    element = document.getElementById('gmap');



    navigator.geolocation.getCurrentPosition(function(position)
    {
    	

    	
    	var visitor = {lat: position.coords.latitude, 
    				   lng: position.coords.longitude};

    	google.maps.visualRefresh = true;	

    	//creating map object, bound, info window and direction service			   
    	options = {
    		center: visitor, 
    		zoom : 10,
    		mapTypeId : google.maps.MapTypeId.ROADMAP
    	};

    	
    	var map = new google.maps.Map(element,options);
    	var bounds = new google.maps.LatLngBounds();
    	var infowindow = new google.maps.InfoWindow({ content : "<b>You</b>" });
    	var directionsService = new google.maps.DirectionsService();


    	//setting marker for visitor
    	options = {position : visitor, map : map};
    	var visitor_marker = new google.maps.Marker(options);
    	infowindow.open(map,visitor_marker);
    	bounds.extend(visitor);



		// for each location setting marker and route

    	for(var i = 0; i<locations.length; i++)
    	{
    		options = {position : locations[i], map : map};

	    	// var target_marker = new google.maps.Marker(options);
	    	// console.log(locations[i]);
	    	// bounds.extend(locations[i]);

	    	var directionsRequest = {
			  origin: visitor,
			  destination: locations[i],
			  travelMode: google.maps.DirectionsTravelMode.DRIVING,
			  unitSystem: google.maps.UnitSystem.METRIC
			};

			directionsService.route(
			  directionsRequest,
			  function(response, status)
			  {
			    if (status == google.maps.DirectionsStatus.OK)
			    {
			      new google.maps.DirectionsRenderer({
			        map: map,
			        directions: response,
			        polylineOptions: {
				      strokeColor: "red",
				      // strokeWeight: 5
				    }
			      });
			    }
			    else{
			    	console.log("Could not find the route");
			    }
			  }
			);

    	}
 	
    	map.fitBounds(bounds);

    }
    );
}
);