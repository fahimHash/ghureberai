function addCorToSearchForm(ds,lat,lon)
{
    $("#searchForm input[name=dis]").attr('value',ds);
    $("#searchForm input[name=lat]").attr('value',lat);
    $("#searchForm input[name=lon]").attr('value',lon);
}

function addCorToBDSearchForm(lat,lon)
{
    $("#bdSearch input[name=lat]").attr('value',lat);
    $("#bdSearch input[name=lon]").attr('value',lon);
}

function addLocationToHtml(address)
{
    $("#wait").remove();                
    $("#div").html(address); 
}

function addCorToNearbyForm(dv,ds,lat,lon)
{
    $("#nearbyForm input[name=div]").attr('value',dv);
    $("#nearbyForm input[name=dis]").attr('value',ds);
    $("#nearbyForm input[name=lat]").attr('value',lat);
    $("#nearbyForm input[name=lon]").attr('value',lon);
}

function sendLocationTrackerData(lat,lon,div,dis)
{
    console.log("ok");
    $.ajax({
        method: 'get',
        data: {'lat' : lat, 'lon' : lon, 'div' : div, 'dis' : dis},
        dataType: 'json',
        url: 'http://localhost/larajects/ghureBerai04/public/trackVisitor'
    }).done(function(data) {
       
    }).fail(function(error) {
        
    });
}







$(document).ready(function(){

    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(function(position)
        {   

            

            var geocoder = new google.maps.Geocoder();
            var geolocate = new google.
                                maps.
                                LatLng(position.coords.latitude, 
                                       position.coords.longitude);
            // var geolocate = new google.
            //                     maps.
            //                     LatLng(23.744969, 90.481913);


            visitor_lt = position.coords.latitude;
            visitor_ln = position.coords.longitude;



            
            //Showing the visitor on home page map-----------------------------------------------

            var style = 
            [
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#193341"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#2c5a71"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#29768a"
                        },
                        {
                            "lightness": -37
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#406d80"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#406d80"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#3e606f"
                        },
                        {
                            "weight": 2
                        },
                        {
                            "gamma": 0.84
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "weight": 0.6
                        },
                        {
                            "color": "#1a3541"
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#2c5a71"
                        }
                    ]
                }
            ]

            var visitor_map = new google.maps.Map(document.getElementById('visitor_map'), {
              zoom: 15,
              center: {lat: visitor_lt, lng: visitor_ln},
              styles: style
            });

            var visitor_marker_on_home_map = new google.maps.Marker({
              position: {lat: visitor_lt, lng: visitor_ln},
              map: visitor_map,
            });

            var visitor_infowindow = new google.maps.InfoWindow({ content : "<b>You</b>" });
            visitor_infowindow.open(visitor_map,visitor_marker_on_home_map);

            //End of Showing the visitor on home page map-----------------------------------------
            




            geocoder.geocode({'latLng': geolocate},function(results, status)
            {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    var result;
                    
                    console.log("you");
                    console.log(results);
                    console.log("you");

                    var length = results.length;
                    var fullAddress;

                    for(var count = 0;count<length;count++)
                    {
                        
                        var pad = results[count].formatted_address;
                        pad = pad.split(',')[0];

                        fullAddress = fullAddress+","+pad;

                    }

                    var dv = (fullAddress.match(/([A-z])+\sDivision/g)[0]).replace("Division","");
                    var ds = (fullAddress.match(/([A-z])+\sDistrict/g)[0]).replace("District","");
                    
                    

                    fullAddress = fullAddress.replace("undefined," , "");
                    fullAddress = fullAddress.replace("Unnamed Road," , "");
                    fullAddress = fullAddress.replace(" District" , "");
                    fullAddress = fullAddress.replace(" Division" , "");

                    //the address need to be smarter

                    // console.log(fullAddress);

                    addCorToNearbyForm(dv,ds,position.coords.latitude,position.coords.longitude);
                    addCorToSearchForm(ds,position.coords.latitude,position.coords.longitude);
                    addCorToBDSearchForm(position.coords.latitude,position.coords.longitude);
                    addLocationToHtml(fullAddress);
                    
                    
                    sendLocationTrackerData(position.coords.latitude,position.coords.longitude,dv,ds);

                    
                    
                }

            }
            );
        }
        );
    }else
    {
        $('#location').html('Geolocation is not supported by this browser.');
    }
}
);
