<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location_name');
            $table->text('location_name_bangla');
            
            $table->integer('division_id')->unsigned();
            $table->foreign('division_id')->references('id')->on('divisions');

            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('districts');

            $table->integer('upazila_id')->unsigned();
            $table->foreign('upazila_id')->references('id')->on('upazilas');



            $table->binary('location_pic');
            $table->integer('cost_transport');
            $table->integer('cost_accommodation');
            $table->text('location_info');
            $table->double('latitude');
            $table->double('longitude');
                       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
