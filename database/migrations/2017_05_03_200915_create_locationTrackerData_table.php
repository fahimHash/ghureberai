<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTrackerDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_tracker_data', function (Blueprint $table) {
            $table->increments('id');
            $table->double('lat', 10, 8);
            $table->double('lon', 10, 8);
            $table->string('district');
            $table->string('division');
            $table->string('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locationTrackerData', function (Blueprint $table) {
            Schema::dropIfExists('locationTrackerData');
        });
    }
}
