<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLocationsTableCollumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
         
            
            $table->string('location_type')->nullable()->change();
            $table->string('location_pic')->nullable()->change();
            $table->string('transportation')->nullable()->change();
            $table->integer('rating')->nullable()->unsigned()->change();
            $table->integer('visited')->nullable()->unsigned()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
