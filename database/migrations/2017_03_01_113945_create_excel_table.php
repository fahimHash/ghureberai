<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExcelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location_name');
            $table->text('location_name_bangla');
            
            $table->integer('division_id')->unsigned();
        

            $table->integer('district_id')->unsigned();
           

            $table->integer('upazila_id')->unsigned();
        



            $table->binary('location_pic');
            $table->integer('cost_transport');
            $table->integer('cost_accommodation');
            $table->text('location_info');
                       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
